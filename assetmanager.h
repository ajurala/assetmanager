#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QList>
#include <QFile>
#include <QtSql>
#include <QStyledItemDelegate>
#include <QCheckBox>

#include "assettreemodel.h"
#include "assetfilter.h"


namespace Ui {
class AssetManager;
class NewInformation;
class ChartOptions;
}

class AssetManager : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit AssetManager(QWidget *parent = 0);
    ~AssetManager();
    
private slots:
    void doubleClicked( const QModelIndex &index );
    void tabCloseRequested(int index);
    void sectionResized ( int logicalIndex, int oldSize, int newSize );

    void newUserAccount();
    void newCategory();
    void newInstrument();
    void newTransaction();

    void newChart();

    void instrumentSelected(int index);
    void saveDatabase();

    void insertNew();
    void removeRow();
    void modifyRow();

    void viewStateChanged(int state);
    void chartTypeChanged(int state);
    void legendChanged(int state);

    void hideRiskChanged(bool state);

    void selectAllUserAccount(bool checked);
    void selectAllCategory(bool checked);
    void selectAllInstrument(bool checked);
    void selectAllRisk(bool checked);

private:

    void readSettings();

    void closeEvent(QCloseEvent *event);

    void setChartType(AssetFilter *afilter, int state, bool updateLegend = true);
    void setLegend(AssetFilter *afilter, int state, bool updateChartType = true);

    void setAllChartType(int state);
    void setAllLegend(int state);

    void updateUserAccount(QModelIndex &index);
    void updateCategory(QModelIndex &index);
    void updateInstrument(QModelIndex &index);
    void updateTransaction(QModelIndex &index);

    void removeUserAccount(QModelIndex &index);
    void removeCategory(QModelIndex &index);
    void removeInstrument(QModelIndex &index);
    void removeTransaction(QModelIndex &index);

    int showUserAccountDialog();
    int showCategoryDialog();
    int showInstrumentDialog();
    int showTransactionDialog();

    void updateTreeView();
    void resizeColumnsToContents();

    void expandAll();
private:
    Ui::AssetManager *ui;
    AssetTreeModel *atm;

    Ui::NewInformation *ni;
    QDialog *niDialog;
    RowType dialogType;

    Ui::ChartOptions *co;
    QDialog *coDialog;

    QCheckBox *checkBox;
    QCheckBox *chartCheckBox;
    QCheckBox *legendCheckBox;

    ModelState state;
    bool hideRisk;

    QList<AssetFilter *> assetFilterList;

    QMap<int, int> columnWidth;

    QList<QCheckBox *> userAccountsCheckBoxList;
    QList<QCheckBox *> categoryCheckBoxList;
    QList<QCheckBox *> instrumentCheckBoxList;
    QList<QCheckBox *> riskCheckBoxList;
};


//class NewInformation

#endif // ASSETMANAGER_H
