#ifndef ASSETTREEMODEL_H
#define ASSETTREEMODEL_H

#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QList>
#include <QFile>
#include <QtSql>
#include <QStyledItemDelegate>
#include <QPainter>

#define ROOT_NAME "Assets"

#define stringize(x) #x

enum ModelState {
    NORMAL = 0,
    MINIMAL,
    FULL
};

enum FontSize {
    ROOT_FONT = 3,
    TRANSACTIONS_FONT = 4,
    RISK_FONT = 1,
    CATEGORY_FONT = 1,
    USER_ACCOUNT_FONT = 7,
    HEADER_FONT = 3
};

//PPU - Price Per Unit
enum TableSchema {
    NAME = 0,
    ID,
    DESCRIPTION,
    INSTRUMENT_RISK,
    INSTRUMENT_ROI,
    INSTRUMENT_CURRENT_PPU,
    RESERVED3,
    RESERVED4,
    USER_ACCOUNT,
    CATEGORY,
    INSTRUMENT,
    RISK,
    BOUGHT_DATE,
    END_DATE,
    UNITS,
    PRICE_PER_UNIT,
    TOTAL_INVESTMENT,
    RATE_OF_INTEREST,
    CURRENT_TOTAL,
    CURRENT_TOTAL_ROI,
    RESERVED5,
    RESERVED6,
    RESERVED7,
    RESERVED8,
    RESERVED9,
    HISTORY,
    SCHEMA_END
};

enum RowType {
    ROOT,
    USERACCOUNT_TYPE,
    CATEGORY_TYPE,
    RISK_TYPE,
    INSTRUMENT_TYPE,
    TRANSACTIONS_TYPE,
    END_TYPE
};

enum EditMode {
    NEW,
    EDIT
};


class DoubleValueColumnDelegate : public QStyledItemDelegate {
    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    virtual QString displayText(const QVariant &value, const QLocale &locale) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const;

};

class DateColumnDelegate : public QStyledItemDelegate {
    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    virtual QString displayText(const QVariant &value, const QLocale &locale) const;

    void paint(QPainter *painter,
               const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class TextColumnDelegate : public QStyledItemDelegate {
    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    virtual QString displayText(const QVariant &value, const QLocale &locale) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const;
};

class FilterProxy;

class RowInfo {
public:
    RowType type;
    RowInfo *parent;
    QSharedPointer<FilterProxy> filterProxy;
    int myRow;
    QList<RowInfo> children;

    bool hasLeafTransactions;

    double totalInvestment;
    double currentTotal;
    double currentTotalROI;

    RowInfo();
    ~RowInfo();
};

class Filter
{
public:
    int colId;
    int defaultId;
    QList<int> filterIds;
    Filter();
};

class SqlRelationalTableModel: public QSqlRelationalTableModel
{
public:
    SqlRelationalTableModel(QObject *parent = 0,
                                          QSqlDatabase db = QSqlDatabase());
    ~SqlRelationalTableModel();

    //void setTable(const QString &tableName);
    //virtual bool select();
private:
    //QSqlTableModel *sqlModel;
};

class FilterProxy: public QSortFilterProxyModel
{
public:
    FilterProxy(QList<Filter> fList, RowType rType,QObject *parent = 0);

    RowType type;
    QList<Filter> filterList;

    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

    bool setDefaultValues(int row, int count);

    QVariant getData(int id, int column);
    QModelIndex getIndex(int id);
    int getId(int column, QVariant value);
};


class AssetTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    AssetTreeModel(QString dbName, QObject *parent = 0);
    ~AssetTreeModel();

    void sortAll();
    void updateRowInformation();
    int getInstrumentId(QModelIndex ind);
    void setState(ModelState state);
    void setHideRisk(bool hide);

    void beginResetModel();
    void endResetModel();

    bool dirtyState();
    void resetDirtyState(bool dirt = false);


    QModelIndex getIndex(int row, int column,
                              const QModelIndex &parent = QModelIndex(),
                              bool forceCreateIndex = false) const;

    QVariant getData(const QModelIndex &index, int role = Qt::DisplayRole, bool forceDefaultData = false) const;

    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    RowType indexType(const QModelIndex &index);

private:
    void createRelationalTables(QSqlDatabase &db);
    void updateDBTableModel(QSqlDatabase &db);

    void updateCalculatedRows(QModelIndex index, RowInfo *rInfo, FilterProxy *proxy, QVariant oldData, QVariant data);
    void updateParentsRowInformation(QModelIndex index, double diff, int column);

public:
    RowInfo rootRow;

    QSharedPointer<FilterProxy> userAccountProxy;
    QSharedPointer<FilterProxy> categoryProxy;
    QSharedPointer<FilterProxy> riskProxy;
    QSharedPointer<FilterProxy> instrumentProxy;
    QSharedPointer<FilterProxy> transactionsProxy;

    QSqlTableModel *userAccountModel;
    QSqlTableModel *categoryModel;
    QSqlTableModel *riskModel;
    QSqlTableModel *instrumentModel;
    SqlRelationalTableModel *transactionsModel;

private:
    QFont defaultFont;
    ModelState state;

    bool dirty;

    bool hideRisk;
};


#endif // ASSETTREEMODEL_H
