#ifndef ASSETPIETAB_H
#define ASSETPIETAB_H

#include "nightcharts.h"

#include <QWidget>

class AssetPieItem
{
    double value;
    double percentage;
    QString name;
    QColor color;

    public:
    void setValue(double Value);
    void setPercentage(double Percentage);
    void setName(QString Name);
    void setColor(QColor Color);

    double getValue();
    double getPercentage();
    QString getName();
    QColor getColor();

};

class AssetPieTab : public QWidget
{
    Q_OBJECT
public:
    explicit AssetPieTab(QWidget *parent = 0);
    void paintEvent(QPaintEvent *e);

    void setPieType(Nightcharts::type Type);
    void setLegendType(Nightcharts::legend_type LType);

    void clearItemList();

    void addItemInfo(double Value, double Percentage, QString Name, QColor Color);


signals:

public slots:

private:

    Nightcharts::legend_type legendType;
    Nightcharts::type pieType;

    QList <AssetPieItem> itemList;
};

#endif // ASSETPIETAB_H
