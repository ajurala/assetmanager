#ifndef COMMONINITIALIZERS_H
#define COMMONINITIALIZERS_H

#include "assettreemodel.h"

QString headerInfo[] = {
    stringize(NAME),
    stringize(ID),
    stringize(DESCRIPTION),
    stringize(INSTRUMENT_RISK),
    stringize(INSTRUMENT_ROI),
    stringize(INSTRUMENT_CURRENT_PPU),
    stringize(RESERVED3),
    stringize(RESERVED4),
    stringize(USER_ACCOUNT),
    stringize(CATEGORY),
    stringize(INSTRUMENT),
    stringize(RISK),
    stringize(BOUGHT_DATE),
    stringize(END_DATE),
    stringize(UNITS),
    stringize(PRICE_PER_UNIT),
    stringize(TOTAL_INVESTMENT),
    stringize(RATE_OF_INTEREST),
    stringize(CURRENT_TOTAL),
    stringize(CURRENT_TOTAL_ROI),
    stringize(RESERVED5),
    stringize(RESERVED6),
    stringize(RESERVED7),
    stringize(RESERVED8),
    stringize(RESERVED9),
    stringize(HISTORY)
};

#endif // COMMONINITIALIZERS_H
