#include "assetpietab.h"
#include <QThread>
#include <QtDebug>


AssetPieTab::AssetPieTab(QWidget *parent) :
    QWidget(parent)
{
    legendType = Nightcharts::Round;
    pieType = Nightcharts::DPie;
}

void AssetPieTab::paintEvent(QPaintEvent *e)
{
    //qDebug()<<"Painting to widget parent starts now";
    QWidget::paintEvent(e);
    QPainter painter;
    //qDebug()<<"Painting begins now";

    int userX = 10;
    int userY = 75; //(this->height() - (this->height()/1.3))/2;
    int userWidth =  this->width()/1.3;
    int userHeight = this->height()/1.3;

    if(legendType == Nightcharts::Round)
    {
        userX = this->width()/6;
        userWidth = this->width()/1.5;
    }


    painter.begin(this);

    Nightcharts pieChart;
    pieChart.setType(pieType);//{Histogram,Pie,DPie};
    pieChart.setLegendType(legendType);//{Round,Vertical}
    pieChart.setCords(userX,userY,userWidth,userHeight);

    for(int i = 0; i < itemList.count(); ++i)
    {
       pieChart.addPiece(itemList[i].getName(),itemList[i].getColor(),
                                 itemList[i].getPercentage(), itemList[i].getValue());
    }

    pieChart.draw(&painter);
    pieChart.drawLegend(&painter);

}

void AssetPieTab::setPieType(Nightcharts::type Type)
{
    pieType = Type;
}

void AssetPieTab::setLegendType(Nightcharts::legend_type LType)
{
    legendType = LType;
}

void AssetPieTab::clearItemList()
{
    itemList.clear();
}

void AssetPieTab::addItemInfo(double Value, double Percentage, QString Name, QColor Color)
{
    AssetPieItem assetPieItem;

    assetPieItem.setValue(Value);
    assetPieItem.setPercentage(Percentage);
    assetPieItem.setName(Name);
    assetPieItem.setColor(Color);

    itemList.append(assetPieItem);
}

void AssetPieItem::setValue(double Value)
{
    value = Value;
}

void AssetPieItem::setPercentage(double Percentage)
{
    percentage = Percentage;
}

void AssetPieItem::setName(QString Name)
{
    name = Name;
}

void AssetPieItem::setColor(QColor Color)
{
    color = Color;
}


double AssetPieItem::getValue()
{
    return value;
}

double AssetPieItem::getPercentage()
{
    return percentage;
}

QString AssetPieItem::getName()
{
    return name;
}

QColor AssetPieItem::getColor()
{
    return color;
}
