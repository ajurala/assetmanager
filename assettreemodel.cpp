#include "assettreemodel.h"
#include "commoninitializers.h"


/**************************************************************/
RowInfo::RowInfo()
{
    parent = 0;
    //childrenFilterProxy = 0;
    type = ROOT;
    myRow = -1;
    hasLeafTransactions = false;
}

RowInfo::~RowInfo()
{
    //if(childrenFilterProxy)
        //delete childrenFilterProxy;
    filterProxy.clear();
    children.clear();
    //qDebug()<<"Yeah ...";
}

/**************************************************************/
AssetTreeModel::AssetTreeModel(QString dbName, QObject *parent)
    : QAbstractItemModel(parent)
{

    hideRisk = false;
    state = NORMAL;
    /* Check for the file existence,
    if does not exist then create a new one with default Tables */

    bool dbExists = QFile::exists(dbName);

    //qDebug()<<dbExists <<"Yeah";
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE", dbName);

    db.setDatabaseName(dbName);
    if (!db.open()) {
        qDebug()<<"BIG ERROR MATE DB Not Open ...";
        qDebug()<<db.lastError();
        return;
    }

    if(!dbExists)
    {
        createRelationalTables(db);
    }

    /* Create the model and Update the initial Proxies that we need */
    qDebug()<<"Transaction begin status"<<db.transaction();

    updateDBTableModel(db);

    /* Sort All the Tables, which updates the Row Information too */
    sortAll();

    /* Update all the Row Information */
    //updateRowInformation();

    dirty = false;
}

AssetTreeModel::~AssetTreeModel()
{
    userAccountProxy.clear();
    categoryProxy.clear();
    riskProxy.clear();
}

void AssetTreeModel::setState(ModelState modelState)
{
    state = modelState;
}

void AssetTreeModel::setHideRisk(bool hide)
{
    hideRisk = hide;
}

void AssetTreeModel::createRelationalTables(QSqlDatabase &db)
{
    QSqlQuery query(db);

    bool ret = query.exec("create table USERACCOUNTS (NAME varchar(20) not null unique,"
                          "id integer primary key autoincrement not null unique, "
                          "DESCRIPTION blob)");

    qDebug()<<"Query to create USERACCOUNTS: "<<ret<<query.lastError()  ;

    ret = query.exec("insert into USERACCOUNTS values('Misc', 0,'Miscellaneous')");

    qDebug()<<"Query to populate USERACCOUNTS: "<<ret<<query.lastError()  ;

    ret = query.exec("create table CATEGORY (NAME varchar(20) not null unique, "
                     "id integer primary key autoincrement not null unique, "
                     "DESCRIPTION blob)");

    qDebug()<<"Query to create CATEGORY: "<<ret<<query.lastError()  ;

    ret = query.exec("insert into CATEGORY values('Misc', 0, 'Miscellaneous')");

    qDebug()<<"Query to populate CATEGORY: "<<ret<<query.lastError()  ;

    ret = query.exec("create table RISK (NAME varchar(20) not null, "
                     "id integer primary key autoincrement not null unique, "
                     "DESCRIPTION blob)");

    qDebug()<<"Query to create RISK: "<<ret<<query.lastError()  ;

    ret = query.exec("insert into RISK values('No Risk', 0, '')");
    ret = query.exec("insert into RISK values('Low Risk', 1, '')");
    ret = query.exec("insert into RISK values('Medium Risk', 2, '')");
    ret = query.exec("insert into RISK values('High Risk', 3, '')");
    ret = query.exec("insert into RISK values('Very High Risk', 4, '')");

    qDebug()<<"Query to populate RISK: "<<ret<<query.lastError()  ;

    ret = query.exec("create table INSTRUMENT (NAME varchar(20) not null unique, "
                     "id integer primary key autoincrement not null unique, "
                     "DESCRIPTION blob, "
                     "INSTRUMENT_RISK int, "
                     "RATE_OF_INTEREST float, "
                     "CURRENT_PPU float "
                     ")");

    qDebug()<<"Query to create INSTRUMENT: "<<ret<<query.lastError()  ;

    ret = query.exec("insert into INSTRUMENT values('Misc', 0, 'Miscellaneous', 0, 0.0, 0.0)");

    qDebug()<<"Query to populate INSTRUMENT: "<<ret<<query.lastError()  ;

    ret = query.exec("create table TRANSACTIONS (NAME varchar(20), "
                     "id integer primary key autoincrement not null unique, "
                     "DESCRIPTION blob, "
                     "RESERVED0 blob, ""RESERVED1 blob, "
                     "RESERVED2 blob, ""RESERVED3 blob, "
                     "RESERVED4 blob, "
                     "USER_ACCOUNT integer, ""CATEGORY integer, "
                     "RISK integer, ""INSTRUMENT integer, "
                     "BOUGHT_DATE date, ""END_DATE date, "
                     "UNITS float, ""PRICE_PER_UNIT float, "
                     "TOTAL_INVESTMENT float, ""RATE_OF_INTEREST float, "
                     "CURRENT_TOTAL float, ""CURRENT_TOTAL_ROI float, "
                     "RESERVED5 blob, ""RESERVED6 blob, "
                     "RESERVED7 blob, ""RESERVED8 blob, "
                     "RESERVED9 blob, "
                     "HISTORY blob "
                     ")");

    qDebug()<<"Query to create TRANSACTIONS: "<<ret<<query.lastError() ;
}

void AssetTreeModel::updateDBTableModel(QSqlDatabase &db)
{
    QSqlTableModel::EditStrategy editStrategy = QSqlTableModel::OnManualSubmit;

    userAccountModel = new QSqlTableModel(this, db);
    userAccountModel->setTable("USERACCOUNTS");
    userAccountModel->setEditStrategy(editStrategy); //OnManualSubmit //OnFieldChange
    userAccountModel->select();

    categoryModel = new QSqlTableModel(this, db);
    categoryModel->setTable("CATEGORY");
    categoryModel->setEditStrategy(editStrategy); //OnManualSubmit //OnFieldChange
    categoryModel->select();

    riskModel = new QSqlTableModel(this, db);
    riskModel->setTable("RISK");
    riskModel->setEditStrategy(editStrategy); //OnManualSubmit //OnFieldChange
    riskModel->select();

    instrumentModel = new QSqlTableModel(this, db);
    instrumentModel->setTable("INSTRUMENT");
    instrumentModel->setEditStrategy(editStrategy); //OnManualSubmit //OnFieldChange
    instrumentModel->select();

    transactionsModel = new SqlRelationalTableModel(this, db);
    transactionsModel->setTable("TRANSACTIONS");
    transactionsModel->setEditStrategy(editStrategy); //OnManualSubmit //OnFieldChange
    //transactionsModel->setRelation(INSTRUMENT, QSqlRelation("INSTRUMENT", "id", "NAME"));
    //transactionsModel->setRelation(RISK, QSqlRelation("INSTRUMENT", "id", "INSTRUMENT_RISK"));
    transactionsModel->select();

    //instrumentModel = transactionsModel->relationModel(INSTRUMENT);
    //instrumentModel->setEditStrategy(editStrategy);

    Filter filter;
    filter.colId = ID;

    FilterProxy *proxy;

    QList<Filter> userAccountFilter;
    userAccountFilter.append(filter);
    userAccountProxy = QSharedPointer<FilterProxy>(new FilterProxy(userAccountFilter, USERACCOUNT_TYPE, this));
    proxy = ((FilterProxy *)userAccountProxy.data());
    proxy->setSourceModel(userAccountModel);

    QList<Filter > categoryFilter;
    categoryFilter.append(filter);
    categoryProxy = QSharedPointer<FilterProxy>(new FilterProxy(categoryFilter, CATEGORY_TYPE, this));
    proxy = ((FilterProxy *)categoryProxy.data());
    proxy->setSourceModel(categoryModel);

    QList<Filter > riskFilter;
    riskFilter.append(filter);
    riskProxy = QSharedPointer<FilterProxy>(new FilterProxy(riskFilter, RISK_TYPE, this));
    proxy = ((FilterProxy *)riskProxy.data());
    proxy->setSourceModel(riskModel);

    QList<Filter > instrumentFilter;
    instrumentFilter.append(filter);
    instrumentProxy = QSharedPointer<FilterProxy>(new FilterProxy(instrumentFilter, INSTRUMENT_TYPE, this));
    proxy = ((FilterProxy *)instrumentProxy.data());
    proxy->setSourceModel(instrumentModel);

    QList<Filter > transactionsFilter;
    transactionsFilter.append(filter);
    transactionsProxy = QSharedPointer<FilterProxy>(new FilterProxy(transactionsFilter, TRANSACTIONS_TYPE, this));
    proxy = ((FilterProxy *)transactionsProxy.data());
    proxy->setSourceModel(transactionsModel);

}

void AssetTreeModel::sortAll()
{
    FilterProxy *proxy;

    proxy = ((FilterProxy *)userAccountProxy.data());
    proxy->sort(NAME);

    proxy = ((FilterProxy *)categoryProxy.data());
    proxy->sort(NAME);

    proxy = ((FilterProxy *)instrumentProxy.data());
    proxy->sort(NAME);

    updateRowInformation();
}

void AssetTreeModel::updateRowInformation()
{
    QAbstractItemModel::beginResetModel();
    rootRow.children.clear();

    rootRow.type = ROOT;
    rootRow.parent = 0;
    rootRow.myRow = 0;
    rootRow.filterProxy = userAccountProxy;

    rootRow.totalInvestment = rootRow.currentTotal = rootRow.currentTotalROI = 0;

    FilterProxy *uproxy = (FilterProxy *)(userAccountProxy.data());
    FilterProxy *cproxy = (FilterProxy *)(categoryProxy.data());
    FilterProxy *rproxy = (FilterProxy *)(riskProxy.data());
    FilterProxy *iproxy = (FilterProxy *)(instrumentProxy.data());

    uproxy->type = USERACCOUNT_TYPE;
    for(int i = 0; i < uproxy->rowCount(); i++)
    {
        RowInfo rCatInfo;
        rCatInfo.type = USERACCOUNT_TYPE;
        rCatInfo.parent = &rootRow;
        rCatInfo.myRow = i;
        rCatInfo.totalInvestment = rCatInfo.currentTotal = rCatInfo.currentTotalROI = 0;
        if(state == MINIMAL)
        {
            QList<Filter > transactionsFilter;
            Filter filter;

            filter.colId = USER_ACCOUNT;
            filter.filterIds.append(uproxy->data(uproxy->index(i, ID)).toInt());
            filter.defaultId = filter.filterIds[0];
            transactionsFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = CATEGORY;
            filter.defaultId = cproxy->getId(NAME, "Misc");
            transactionsFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = RISK;
            filter.defaultId = rproxy->getId(NAME, "Misc");
            transactionsFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = INSTRUMENT;
            filter.defaultId = iproxy->getId(NAME, "Misc");
            transactionsFilter.append(filter);

            rCatInfo.filterProxy =  QSharedPointer<FilterProxy>(new FilterProxy(transactionsFilter, TRANSACTIONS_TYPE, this));
            FilterProxy *tproxy = (FilterProxy *)(rCatInfo.filterProxy.data());

            tproxy->setSourceModel(transactionsModel);
            tproxy->sort(NAME);

            tproxy->type = TRANSACTIONS_TYPE;
            rootRow.children.append(rCatInfo);

            for(int j = 0; j < tproxy->rowCount(); j++)
            {
                RowInfo tTransInfo;
                tTransInfo.type = TRANSACTIONS_TYPE;
                tTransInfo.parent = &(rootRow.children[i]);
                tTransInfo.myRow = j;

                tTransInfo.totalInvestment = tproxy->data(tproxy->index(j, TOTAL_INVESTMENT)).toDouble();
                tTransInfo.currentTotal = tproxy->data(tproxy->index(j, CURRENT_TOTAL)).toDouble();
                tTransInfo.currentTotalROI = tproxy->data(tproxy->index(j, CURRENT_TOTAL_ROI)).toDouble();

                tTransInfo.parent->totalInvestment += tTransInfo.totalInvestment;
                tTransInfo.parent->currentTotal += tTransInfo.currentTotal;
                tTransInfo.parent->currentTotalROI += tTransInfo.currentTotalROI;
                tTransInfo.parent->hasLeafTransactions = true;

                tTransInfo.hasLeafTransactions = true; // in my case i am the leaf transaction :-)

                rootRow.children[i].children.append(tTransInfo);
            }
        }
        else
        {
            rCatInfo.filterProxy = categoryProxy;
            cproxy->type = CATEGORY_TYPE;
            rootRow.children.append(rCatInfo);

            for(int j = 0; j < cproxy->rowCount(); j++)
            {
                RowInfo rRiskInfo;
                rRiskInfo.type = CATEGORY_TYPE;
                rRiskInfo.parent = &(rootRow.children[i]);
                rRiskInfo.myRow = j;
                rRiskInfo.totalInvestment = rRiskInfo.currentTotal = rRiskInfo.currentTotalROI = 0;

                if(state == NORMAL && hideRisk == true)
                {
                    QList<Filter > transactionsFilter;
                    Filter filter;

                    filter.colId = USER_ACCOUNT;
                    filter.filterIds.append(uproxy->data(uproxy->index(i, ID)).toInt());
                    filter.defaultId = filter.filterIds[0];
                    transactionsFilter.append(filter);

                    filter.filterIds.clear();
                    filter.colId = CATEGORY;
                    filter.filterIds.append(cproxy->data(cproxy->index(j, ID)).toInt());
                    filter.defaultId = filter.filterIds[0];
                    transactionsFilter.append(filter);

                    filter.filterIds.clear();
                    filter.colId = INSTRUMENT;
                    int instrumentId = iproxy->getId(NAME, "Misc");
                    filter.defaultId = instrumentId;
                    transactionsFilter.append(filter);

                    filter.filterIds.clear();
                    filter.colId = RISK;
                    filter.defaultId = iproxy->getData(instrumentId, INSTRUMENT_RISK).toInt();
                    transactionsFilter.append(filter);

                    rRiskInfo.filterProxy =  QSharedPointer<FilterProxy>(new FilterProxy(transactionsFilter, TRANSACTIONS_TYPE, this));
                    FilterProxy *tproxy = (FilterProxy *)(rRiskInfo.filterProxy.data());

                    tproxy->setSourceModel(transactionsModel);
                    tproxy->sort(NAME);

                    tproxy->type = TRANSACTIONS_TYPE;
                    rootRow.children[i].children.append(rRiskInfo);

                    for(int k = 0; k < tproxy->rowCount(); k++)
                    {
                        RowInfo tTransInfo;
                        tTransInfo.type = TRANSACTIONS_TYPE;
                        tTransInfo.parent = &(rootRow.children[i].children[j]);
                        tTransInfo.myRow = k;

                        tTransInfo.totalInvestment = tproxy->data(tproxy->index(k, TOTAL_INVESTMENT)).toDouble();
                        tTransInfo.currentTotal = tproxy->data(tproxy->index(k, CURRENT_TOTAL)).toDouble();
                        tTransInfo.currentTotalROI = tproxy->data(tproxy->index(k, CURRENT_TOTAL_ROI)).toDouble();

                        tTransInfo.parent->totalInvestment += tTransInfo.totalInvestment;
                        tTransInfo.parent->currentTotal += tTransInfo.currentTotal;
                        tTransInfo.parent->currentTotalROI += tTransInfo.currentTotalROI;
                        tTransInfo.parent->hasLeafTransactions = true;

                        tTransInfo.hasLeafTransactions = true; // in my case i am the leaf transaction :-)

                        rootRow.children[i].children[j].children.append(tTransInfo);
                    }
                }
                else
                {
                    rRiskInfo.filterProxy = riskProxy;
                    rproxy->type = RISK_TYPE;
                    rootRow.children[i].children.append(rRiskInfo);

                    for(int k = 0; k < rproxy->rowCount(); k++)
                    {
                        RowInfo rTransInfo;
                        rTransInfo.type = RISK_TYPE;
                        rTransInfo.parent = &(rootRow.children[i].children[j]);
                        rTransInfo.myRow = k;

                        rTransInfo.totalInvestment = rTransInfo.currentTotal = rTransInfo.currentTotalROI = 0;

                        QList<Filter > transactionsFilter;
                        Filter filter;

                        filter.colId = USER_ACCOUNT;
                        filter.filterIds.append(uproxy->data(uproxy->index(i, ID)).toInt());
                        filter.defaultId = filter.filterIds[0];
                        transactionsFilter.append(filter);

                        filter.filterIds.clear();
                        filter.colId = CATEGORY;
                        filter.filterIds.append(cproxy->data(cproxy->index(j, ID)).toInt());
                        filter.defaultId = filter.filterIds[0];
                        transactionsFilter.append(filter);

                        filter.filterIds.clear();
                        filter.colId = RISK;
                        filter.filterIds.append(rproxy->data(rproxy->index(k, ID)).toInt());
                        filter.defaultId = filter.filterIds[0];
                        transactionsFilter.append(filter);

                        //FilterProxy *iproxy = (FilterProxy *)instrumentProxy.data();
                        filter.filterIds.clear();
                        filter.colId = INSTRUMENT;
                        filter.defaultId = iproxy->getId(NAME, "Misc");
                        transactionsFilter.append(filter);

                        rTransInfo.filterProxy =  QSharedPointer<FilterProxy>(new FilterProxy(transactionsFilter, TRANSACTIONS_TYPE, this));
                        FilterProxy *tproxy = (FilterProxy *)(rTransInfo.filterProxy.data());

                        tproxy->setSourceModel(transactionsModel);
                        tproxy->sort(NAME);

                        //qDebug()<<"tproxy->rowCount()"<<tproxy->rowCount()<<transactionsModel->rowCount();
                        /*if(tproxy->rowCount() > 0)
                        {
                            qDebug()<<transactionsFilter[0].colId<<transactionsFilter[0].filterIds[0];
                            qDebug()<<transactionsFilter[1].colId<<transactionsFilter[1].filterIds[0];
                            qDebug()<<transactionsFilter[2].colId<<transactionsFilter[2].filterIds[0];
                        }*/

                        tproxy->type = TRANSACTIONS_TYPE;
                        rootRow.children[i].children[j].children.append(rTransInfo);

                        for(int l = 0; l < tproxy->rowCount(); l++)
                        {
                            RowInfo tTransInfo;
                            tTransInfo.type = TRANSACTIONS_TYPE;
                            tTransInfo.parent = &(rootRow.children[i].children[j].children[k]);
                            tTransInfo.myRow = l;

                            tTransInfo.totalInvestment = tproxy->data(tproxy->index(l, TOTAL_INVESTMENT)).toDouble();
                            tTransInfo.currentTotal = tproxy->data(tproxy->index(l, CURRENT_TOTAL)).toDouble();
                            tTransInfo.currentTotalROI = tproxy->data(tproxy->index(l, CURRENT_TOTAL_ROI)).toDouble();

                            tTransInfo.parent->totalInvestment += tTransInfo.totalInvestment;
                            tTransInfo.parent->currentTotal += tTransInfo.currentTotal;
                            tTransInfo.parent->currentTotalROI += tTransInfo.currentTotalROI;
                            tTransInfo.parent->hasLeafTransactions = true;

                            tTransInfo.hasLeafTransactions = true; // in my case i am the leaf transaction :-)

                            rootRow.children[i].children[j].children[k].children.append(tTransInfo);
                        }

                        RowInfo *transInfo = &(rootRow.children[i].children[j].children[k]);
                        transInfo->parent->totalInvestment += transInfo->totalInvestment;
                        transInfo->parent->currentTotal += transInfo->currentTotal;
                        transInfo->parent->currentTotalROI += transInfo->currentTotalROI;
                        transInfo->parent->hasLeafTransactions = transInfo->hasLeafTransactions | transInfo->parent->hasLeafTransactions;
                    }
                }

                RowInfo *transInfo = &(rootRow.children[i].children[j]);
                transInfo->parent->totalInvestment += transInfo->totalInvestment;
                transInfo->parent->currentTotal += transInfo->currentTotal;
                transInfo->parent->currentTotalROI += transInfo->currentTotalROI;
                transInfo->parent->hasLeafTransactions = transInfo->hasLeafTransactions | transInfo->parent->hasLeafTransactions;
            }
        }

        RowInfo *transInfo = &(rootRow.children[i]);
        transInfo->parent->totalInvestment += transInfo->totalInvestment;
        transInfo->parent->currentTotal += transInfo->currentTotal;
        transInfo->parent->currentTotalROI += transInfo->currentTotalROI;
        transInfo->parent->hasLeafTransactions = transInfo->hasLeafTransactions | transInfo->parent->hasLeafTransactions;
    }

    QAbstractItemModel::endResetModel();
}

QModelIndex AssetTreeModel::index(int row, int column,
                          const QModelIndex &parent) const
{
    return getIndex(row, column, parent, state == FULL);
}

QModelIndex AssetTreeModel::getIndex(int row, int column,
                          const QModelIndex &parent, bool forceCreateIndex) const
{
    if(!parent.isValid()) /* ROOT DATA */
    {
        RowInfo *rInfo = const_cast<RowInfo *>(&rootRow);
            return createIndex(row, column, rInfo);
    }
    else
    {
        RowInfo *rInfo = (RowInfo *)parent.internalPointer();
        if(rInfo == 0)
        {
            return QModelIndex(); // Not possible and hence ...
            //return createIndex(row, column, &rootRow);
        }
        else //
        {
            //if(forceCreateIndex)
              //  qDebug()<<"forceCreateIndex is true";

            if(forceCreateIndex
                    || rInfo->children[row].hasLeafTransactions
                    || rInfo->children[row].type == USERACCOUNT_TYPE)
                    /*|| rInfo->children[row].children.count() > 0
                    || rInfo->children[row].totalInvestment > 0
                    || rInfo->children[row].type == TRANSACTIONS_TYPE)*/
            {
                return createIndex(row, column, &(rInfo->children[row]));
            }
        }
    }

    return QModelIndex();
}
QModelIndex AssetTreeModel::parent(const QModelIndex &child) const
{
    if(!child.isValid()) // Some problematic request
        return QModelIndex();

    RowInfo *rInfo = (RowInfo *)child.internalPointer();
    if(rInfo == 0 || rInfo->parent == 0)
        return QModelIndex();
    else
        return createIndex(rInfo->parent->myRow, 0, rInfo->parent);
}

int AssetTreeModel::rowCount(const QModelIndex &parent) const
{
    if(!parent.isValid()) /* ROOT DATA */
        return 1;
    else
    {
        RowInfo *rInfo = (RowInfo *)parent.internalPointer();
        if(rInfo == 0)
        {
            qDebug()<<"It became possible ... NOT";
            return 0; // Not Possible
        }
        else
        {
            FilterProxy *proxy = (FilterProxy *)(rInfo->filterProxy.data());
            if(proxy)
            {
                return proxy->rowCount();
            }
            else
            {
                return 0; // Leaf Node, so no more rows for them
            }
        }
    }
}

int AssetTreeModel::columnCount(const QModelIndex &parent) const
{
    return transactionsModel->columnCount();

    /*
    if(!parent.isValid()) // ROOT DATA
        return transactionsModel->columnCount();
    else
    {
        RowInfo *rInfo = (RowInfo *)parent.internalPointer();
        if(rInfo == 0) // Parent is a Root Row
        {
            return 0; // Not Possible
            //proxy = (FilterProxy *)(rootRow.filterProxy.data());
        }
        else
        {
            FilterProxy *proxy = (FilterProxy *)(rInfo->filterProxy.data());
            return proxy->columnCount();
        }
    }*/
}
QVariant AssetTreeModel::data(const QModelIndex &index, int role) const
{
    return getData(index, role);
}

QVariant AssetTreeModel::getData(const QModelIndex &index, int role, bool forceDefaultData) const
{
    if(index.isValid())
    {
        RowInfo *rInfo = (RowInfo *)index.internalPointer();
        if(rInfo == 0)
        {
            return QVariant(); // Not Possible;
        }
        else if(rInfo->parent == 0)
        {
            if(index.column() == NAME
                    || index.column() == TOTAL_INVESTMENT
                    || index.column() == CURRENT_TOTAL
                    || index.column() == CURRENT_TOTAL_ROI)
            {
                if(role == Qt::DisplayRole)
                {
                    if(index.column() == TOTAL_INVESTMENT)
                        return (rInfo->totalInvestment > 0) ? QString().setNum(rInfo->totalInvestment, 'f', 2) : QVariant();
                    else if(index.column() == CURRENT_TOTAL)
                        return (rInfo->currentTotal > 0) ? QString().setNum(rInfo->currentTotal, 'f', 2) : QVariant();
                    else if(index.column() == CURRENT_TOTAL_ROI)
                        return (rInfo->currentTotalROI > 0) ? QString().setNum(rInfo->currentTotalROI, 'f', 2) : QVariant();
                    else
                        return QVariant(ROOT_NAME);
                }
                else if(role == Qt::FontRole)
                {
                    QFont font = defaultFont;
                    font.setWeight(QFont::Bold);
                    font.setPointSize(font.pointSize()+ROOT_FONT);
                    return QVariant(font);
                }
                else if(role == Qt::TextAlignmentRole)
                {
                    if(index.column() == TOTAL_INVESTMENT
                            || index.column() == CURRENT_TOTAL
                            || index.column() == CURRENT_TOTAL_ROI)
                        return Qt::AlignRight;
                    else
                        return Qt::AlignLeft;
                }
                else if(role == Qt::ForegroundRole)
                {
                    if(index.column() == TOTAL_INVESTMENT
                            || index.column() == CURRENT_TOTAL
                            || index.column() == CURRENT_TOTAL_ROI)
                        return Qt::blue;
                }
            }
            else
                return QVariant();
        }
        else
        {
            if(role == Qt::FontRole)
            {
                QFont font = defaultFont;
                if(rInfo->type == USERACCOUNT_TYPE)
                {
                    font.setStyle(QFont::StyleItalic);
                    font.setPointSize(font.pointSize()+USER_ACCOUNT_FONT);
                }

                if(rInfo->type == CATEGORY_TYPE)
                    font.setPointSize(font.pointSize() + CATEGORY_FONT);

                if(rInfo->type == RISK_TYPE)
                    font.setPointSize(font.pointSize() + RISK_FONT);

                if(rInfo->type == TRANSACTIONS_TYPE)
                {
                    font.setPointSize(font.pointSize() + TRANSACTIONS_FONT);
                }

                if(index.column() == TOTAL_INVESTMENT
                        || index.column() == CURRENT_TOTAL
                        || index.column() == CURRENT_TOTAL_ROI)
                    font.setWeight(QFont::Bold);

                if(rInfo->type != TRANSACTIONS_TYPE && index.column() == NAME)
                    font.setWeight(QFont::Bold);

                return QVariant(font);
            }
            else if(role == Qt::TextAlignmentRole)
            {
                if(index.column() == TOTAL_INVESTMENT
                        || index.column() == CURRENT_TOTAL
                        || index.column() == CURRENT_TOTAL_ROI
                        || index.column() == UNITS
                        || index.column() == PRICE_PER_UNIT
                        || index.column() == RATE_OF_INTEREST)
                    return Qt::AlignRight;
                else if(index.column() == BOUGHT_DATE
                        || index.column() == END_DATE)
                    return Qt::AlignHCenter;
                else return Qt::AlignLeft;
            }
            else if(role == Qt::BackgroundRole)
            {
                if(rInfo->type == TRANSACTIONS_TYPE && index.column() == END_DATE)
                {
                    QDate currentDate = QDate::currentDate();
                    QDate date = data(index).toDate();


                    if(currentDate.daysTo(date) <= 0)
                        return Qt::red;
                    else if(currentDate.daysTo(date) <= 2)
                        return QColor(255, 140, 0);
                    else if(currentDate.daysTo(date) <= 7)
                        return Qt::cyan;
                    else if(currentDate.daysTo(date) <= 14)
                        return Qt::green;

                }

            }            
            else if(role == Qt::ForegroundRole)
            {
                if(index.column() == TOTAL_INVESTMENT
                        || index.column() == CURRENT_TOTAL
                        || index.column() == CURRENT_TOTAL_ROI)
                {
                    if(rInfo->type == CATEGORY_TYPE)
                        return Qt::magenta; //magenta;
                    else if(rInfo->type == RISK_TYPE)
                        return Qt::darkGray;
                }
            }
            else if(role == Qt::DisplayRole)
            {
                if(index.column() == TOTAL_INVESTMENT)
                    return (rInfo->totalInvestment > 0 || rInfo->type == TRANSACTIONS_TYPE) ? QString().setNum(rInfo->totalInvestment, 'f', 2) : QVariant();
                else if(index.column() == CURRENT_TOTAL)
                    return (rInfo->currentTotal > 0 || rInfo->type == TRANSACTIONS_TYPE) ? QString().setNum(rInfo->currentTotal, 'f', 2) : QVariant();
                else if(index.column() == CURRENT_TOTAL_ROI)
                    return (rInfo->currentTotalROI > 0 || rInfo->type == TRANSACTIONS_TYPE) ? QString().setNum(rInfo->currentTotalROI, 'f', 2) : QVariant();
                else
                {
                    FilterProxy *proxy = (FilterProxy *)(rInfo->parent->filterProxy.data());
                    QVariant value = proxy->data(proxy->index(index.row(), index.column()), role);

                    if(!forceDefaultData && index.column() == INSTRUMENT && value.isValid())
                    {
                        FilterProxy *iProxy = (FilterProxy *)instrumentProxy.data();
                        value = iProxy->getData(value.toInt(), NAME);
                    }
                    else if(index.column() == PRICE_PER_UNIT && value.isValid())
                        value = QString().setNum(value.toDouble(), 'f', 2);
                    return value;
                }
            }
        }
    }

    return QVariant();
}
bool AssetTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid())
    {
        RowInfo *rInfo = (RowInfo *)index.internalPointer();
        if(rInfo == 0)
        {
            return true; // Not Possible
        }
        else if(rInfo->parent == 0)
        {
            qDebug()<<"Woah how come u allowed me to set :P";
            return true; // Not allowed to set :-O
        }
        else
        {
            FilterProxy *proxy = (FilterProxy *)(rInfo->parent->filterProxy.data());
            bool ret = false;

            QVariant oldData =  data(index);
            ret = proxy->setData(proxy->index(index.row(), index.column()), value, role);

            if(ret)
            {
                dirty = true;

                emit dataChanged( index, index );
                if(rInfo->type == TRANSACTIONS_TYPE)
                    updateCalculatedRows(index, rInfo, proxy, oldData, value);
            }

            return ret;
        }
    }
    else return true;
}

int AssetTreeModel::getInstrumentId(QModelIndex ind)
{
    /*RowInfo *rInfo = (RowInfo *)ind.internalPointer();
    if(rInfo)
    {
        FilterProxy *proxy = (FilterProxy *)(rInfo->parent->filterProxy.data());
        return transactionsModel->getSQLData(proxy->mapToSource(proxy->index(ind.row(), INSTRUMENT))).toInt();
    }*/

    bool ok;
    int id = getData(index(ind.row(), INSTRUMENT, ind.parent()), Qt::DisplayRole, true).toInt(&ok);
    //qDebug()<<"getInstrumentId"<<ok<<getData(index(ind.row(), INSTRUMENT), Qt::DisplayRole, true);
    return id;

    //return 0;
}

void AssetTreeModel::updateCalculatedRows(QModelIndex ind, RowInfo *rInfo, FilterProxy *proxy, QVariant oldData, QVariant value)
{
    if(value != oldData)
    {
        //Update the row Information of parents
        switch(ind.column())
        {
            case UNITS:
            {
                // update CURRENT_TOTAL;
                double oldCurrentInvestment = proxy->data(proxy->index(ind.row(), CURRENT_TOTAL)).toDouble();

                int instrumentID = getInstrumentId(ind);
                FilterProxy *iProxy = (FilterProxy *)instrumentProxy.data();
                double currentPPU = iProxy->getData(instrumentID, INSTRUMENT_CURRENT_PPU).toDouble();

                if(currentPPU > 0)
                {
                    double newCurrentInvestment = value.toDouble() * currentPPU;

                    double diff = newCurrentInvestment - oldCurrentInvestment;

                    rInfo->currentTotal = newCurrentInvestment;
                    proxy->setData(proxy->index(ind.row(), CURRENT_TOTAL), newCurrentInvestment);
                    QModelIndex toEmitIndex =  index(ind.row(), CURRENT_TOTAL, ind.parent());
                    emit dataChanged(toEmitIndex, toEmitIndex);

                    updateParentsRowInformation(ind, diff, CURRENT_TOTAL);
                }
            }
            case PRICE_PER_UNIT:
            {
                // update TOTAL_INVESTMENT
                double oldTotalInvestment = proxy->data(proxy->index(ind.row(), TOTAL_INVESTMENT)).toDouble();
                double pricePerUnit = ((ind.column() == PRICE_PER_UNIT) ? value.toDouble() : proxy->data(proxy->index(ind.row(), PRICE_PER_UNIT)).toDouble());
                double newTotalInvestment =  pricePerUnit * proxy->data(proxy->index(ind.row(), UNITS)).toDouble();
                double diff = newTotalInvestment - oldTotalInvestment;

                //qDebug()<<"Update Myself first"<<newTotalInvestment<<diff;

                rInfo->totalInvestment = newTotalInvestment;
                proxy->setData(proxy->index(ind.row(), TOTAL_INVESTMENT), newTotalInvestment);
                QModelIndex toEmitIndex =  index(ind.row(), TOTAL_INVESTMENT, ind.parent());
                emit dataChanged(toEmitIndex, toEmitIndex);

                updateParentsRowInformation(ind, diff, TOTAL_INVESTMENT);

            }
            //follow through
            case BOUGHT_DATE:
            case RATE_OF_INTEREST:
            {
                //update CURRENT_TOTAL_ROI only
                double oldCurrentTotalROI = proxy->data(proxy->index(ind.row(), CURRENT_TOTAL_ROI)).toDouble();
                double currentROI = proxy->data(proxy->index(ind.row(), RATE_OF_INTEREST)).toDouble();
                QDate boughtDate = proxy->data(proxy->index(ind.row(), BOUGHT_DATE)).toDate();
                boughtDate = boughtDate.isValid() ? boughtDate : QDate::currentDate();
                int termDuration = boughtDate.daysTo(QDate::currentDate())/365;
                termDuration = termDuration > 0 ? termDuration : 0;

                double newCurrentTotalROI = proxy->data(proxy->index(ind.row(), TOTAL_INVESTMENT)).toDouble() * qPow((1.0 + (currentROI/100.0)), termDuration);

                double diff = newCurrentTotalROI - oldCurrentTotalROI;

                rInfo->currentTotalROI = newCurrentTotalROI;
                proxy->setData(proxy->index(ind.row(), CURRENT_TOTAL_ROI), newCurrentTotalROI);
                QModelIndex toEmitIndex =  index(ind.row(), CURRENT_TOTAL_ROI, ind.parent());
                emit dataChanged(toEmitIndex, toEmitIndex);

                updateParentsRowInformation(ind, diff, CURRENT_TOTAL_ROI);

                /** Check the Current Total now **/
                int instrumentID = getInstrumentId(ind);
                FilterProxy *iProxy = (FilterProxy *)instrumentProxy.data();
                double currentPPU = iProxy->getData(instrumentID, INSTRUMENT_CURRENT_PPU).toDouble();

                if(currentPPU == 0) // Follow ROI
                {
                    double oldCurrentInvestment = proxy->data(proxy->index(ind.row(), CURRENT_TOTAL)).toDouble();
                    double newCurrentInvestment = newCurrentTotalROI;
                    double diff = newCurrentInvestment - oldCurrentInvestment;

                    rInfo->currentTotal = newCurrentInvestment;
                    proxy->setData(proxy->index(ind.row(), CURRENT_TOTAL), newCurrentInvestment);
                    QModelIndex toEmitIndex =  index(ind.row(), CURRENT_TOTAL, ind.parent());
                    emit dataChanged(toEmitIndex, toEmitIndex);

                    updateParentsRowInformation(ind, diff, CURRENT_TOTAL);
                }
            }
            break;

            default:
            break;
        }
    }
}

void AssetTreeModel::updateParentsRowInformation(QModelIndex ind, double diff, int column)
{
    RowInfo *rInfo = (RowInfo *)ind.internalPointer();
    RowInfo *pInfo = rInfo->parent;
    QModelIndex pt = parent(ind);

    while(pInfo)
    {
        QModelIndex toEmitIndex = index(pt.row(), column, parent(pt));
        switch(column)
        {
        case TOTAL_INVESTMENT:
            pInfo->totalInvestment += diff;
            break;
        case CURRENT_TOTAL:
            pInfo->currentTotal += diff;
            break;
        case CURRENT_TOTAL_ROI:
            pInfo->currentTotalROI += diff;
            break;
        default:
            break;
        }
        dataChanged(toEmitIndex, toEmitIndex);

        pt = parent(pt);
        pInfo = pInfo->parent;
    }
}

QVariant AssetTreeModel::headerData(int section, Qt::Orientation orientation,
                            int role) const
{
    if(orientation == Qt::Horizontal)
    {
        if(role == Qt::DisplayRole)
        {
            //QString headerString = headerInfo[section].replace("_", " ");
            /*QStringList headerStringList = headerInfo[section].split("_", QString::SkipEmptyParts);
            for(int i = 0; i < headerStringList.count(); ++i)
            {
                headerStringList[i] = headerStringList[i].toLower();
                headerStringList[i][0] = headerStringList[i].at(0).toTitleCase();
            }

            return headerStringList.join(" ");*/

            return headerInfo[section].replace("_", " ").toLower();
        }
        else if(role == Qt::FontRole)
        {
            QFont font = defaultFont;
            font.setCapitalization(QFont::Capitalize);
            font.setWeight(QFont::Bold);
            font.setPointSize(font.pointSize()+HEADER_FONT);

            return font;
        }
    }

    return QVariant();
}

void AssetTreeModel::beginResetModel()
{
    QAbstractItemModel::beginResetModel();
}

void AssetTreeModel::endResetModel()
{
    sortAll();
    QAbstractItemModel::endResetModel();
}

bool AssetTreeModel::insertRows(int row, int count, const QModelIndex &parent)
{
    RowInfo *rInfo = (RowInfo *)parent.internalPointer();
    if(rInfo == 0)
        return false;
    else
    {
        bool ret = false;
        FilterProxy *proxy = (FilterProxy *)(rInfo->filterProxy.data());

        //beginInsertRows(parent, row, row + count - 1);
        beginResetModel();
        ret = proxy->insertRows(row, count);
        endResetModel();

        if(ret)
            dirty = true;

        return ret;
    }
}
bool AssetTreeModel::removeRows(int row, int count, const QModelIndex &parent)
{
    RowInfo *rInfo = (RowInfo *)parent.internalPointer();
    if(rInfo == 0)
        return false;
    else
    {
        bool ret = false;
        FilterProxy *proxy = (FilterProxy *)(rInfo->filterProxy.data());

        //beginInsertRows(parent, row, row + count - 1);
        beginResetModel();
        ret = proxy->removeRows(row, count);
        endResetModel();

        if(ret)
            dirty = true;
        return ret;
    }
}

Qt::ItemFlags AssetTreeModel::flags(const QModelIndex &index) const
{
    RowInfo *rInfo = (RowInfo *)index.internalPointer();
    if(rInfo == 0
            || rInfo->type == ROOT
            || rInfo->type == USERACCOUNT_TYPE
            || rInfo->type == CATEGORY_TYPE
            || rInfo->type == RISK_TYPE
            || rInfo->type == INSTRUMENT_TYPE)
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    else
    {
        if(index.column() == INSTRUMENT
                ||index.column() == TOTAL_INVESTMENT
                ||index.column() == CURRENT_TOTAL
                ||index.column() == CURRENT_TOTAL_ROI)
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        else
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
    }
}

RowType AssetTreeModel::indexType(const QModelIndex &index)
{
    RowInfo *rInfo = (RowInfo *)index.internalPointer();
    if(rInfo == 0)
        return END_TYPE;
    else
        return rInfo->type;
}

bool AssetTreeModel::dirtyState()
{
    return dirty;
}

void AssetTreeModel::resetDirtyState(bool dirt)
{
    dirty = dirt;
}

/**************************************************************/

Filter::Filter()
{
    colId = defaultId = 0;
}
/**************************************************************/

SqlRelationalTableModel::SqlRelationalTableModel(QObject *parent, QSqlDatabase db)
    : QSqlRelationalTableModel(parent, db)
{
    //sqlModel = new QSqlTableModel(parent, db);
}

SqlRelationalTableModel::~SqlRelationalTableModel()
{
    //delete sqlModel;
}

/*void SqlRelationalTableModel::setTable(const QString &tableName)
{
    QSqlRelationalTableModel::setTable(tableName);
    sqlModel->setTable(tableName);
}*/

/*bool SqlRelationalTableModel::select()
{
    bool ret = QSqlRelationalTableModel::select();

    if(ret)
        return sqlModel->select();
    else
        return ret;
}*/

/**************************************************************/
FilterProxy::FilterProxy(QList<Filter> fList, RowType rType, QObject *parent)
    :QSortFilterProxyModel(parent)
{
    type = rType;
    filterList = fList;
}

bool FilterProxy::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    for(int i = 0; i < filterList.count(); ++i)
    {
        //if count is zero then accept implcitly
        if(filterList[i].filterIds.count() > 0)
        {

            bool ok;
            int data;

            /*if(filterList[i].colId == INSTRUMENT && type == TRANSACTIONS_TYPE)
            {
                SqlRelationalTableModel *model = ((SqlRelationalTableModel *)sourceModel());
                qDebug()<<"SourceModel says rowcount"<<model->rowCount()<<source_row;
                data = model->getSQLData(model->index(source_row, filterList[i].colId)).toInt(&ok);
            }
            else*/
                data = sourceModel()->data(sourceModel()->index(source_row, filterList[i].colId)).toInt(&ok);

            if(ok)
            {
                ok = false;
                for(int j = 0; j < filterList[i].filterIds.count(); ++j)
                {
                    if(filterList[i].filterIds[j] == data)
                    {
                        ok = true;
                        break;
                    }
                }

                if(!ok){
                    //qDebug()<<"Return false"<<filterList[i].colId<<data<<filterList[i].filterIds[0];
                    return false;
                }// No match occurred for this filter list, hence return false
            }
            else
            {
                //qDebug()<<"Invalid ID no match"<<filterList[i].colId;
                return false; //Invalid Id found, no match
            }
        }
    }

    //qDebug()<<"Return true";
    return true;
}

bool FilterProxy::insertRows(int row, int count, const QModelIndex &parent)
{
    //qDebug()<<"insertRows"<<count;
    //bool ret = QSortFilterProxyModel::insertRows(row, count, parent);
    int sourceRow = row >= rowCount() ? sourceModel()->rowCount() : mapToSource(index(row, 0)).row();
    bool ret = sourceModel()->insertRows(sourceRow, count, mapToSource(parent));

    //qDebug()<<"sourceModel()->rowCount"<<sourceModel()->rowCount();
    if(ret == false) qDebug()<<"Ajay - Insert Failed?";

    setDefaultValues(sourceRow, count);
    return ret;
}

bool FilterProxy::setDefaultValues(int row, int count)
{
    bool ret = true;
    //qDebug()<<"Set Default Values";
    for(int i = 0; i < count; ++i)
    {
        for(int j = 0; j < filterList.count(); ++j)
        {
            if(filterList[j].colId != ID)
            {
                //QModelIndex ind = sourceModel()->index(row + i, filterList[j].colId);
                //qDebug()<<ind<<row+i<<filterList[j].colId<<rowCount() ;
                int id = filterList[j].defaultId;

                //Picking the default values for these 2 cols from sql data of the first row in the proxy
                if((filterList[i].colId == RISK
                    || filterList[i].colId == INSTRUMENT) && type == TRANSACTIONS_TYPE)
                {
                    QModelIndex sourceIndex = mapToSource(index(0,filterList[i].colId));
                    if(sourceIndex.isValid())
                    {
                        SqlRelationalTableModel *model = ((SqlRelationalTableModel *)sourceModel());
                        id = model->data(sourceIndex).toInt();
                    }
                    else
                        id = 0; // Currently setting it as 0 as table is empty for now
                }
                ret &= sourceModel()->setData(sourceModel()->index(row + i, filterList[j].colId), filterList[j].defaultId);
            }
            //if(ret == false) qDebug()<<"Ajay - why setData failed ..." <<filterList[j].colId;
        }
    }

    return ret;
}

bool FilterProxy::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    //If the Id of the row is 0 for following types, then it is a 'misc' id, so ignore
    if(type == USERACCOUNT_TYPE
       || type == CATEGORY_TYPE
            || type == INSTRUMENT_TYPE )
    {
        if(sourceModel()->data(index(left.row(), ID)).toInt() == 0)
            return false;
        else if(sourceModel()->data(index(right.row(), ID)).toInt() == 0)
        {
            return true;
        }
    }

    if(left.column() == NAME
       && right.column() == NAME)
    {
        if(sourceModel()->data(left).toString().isEmpty())
        {
                return false;
        }
        else if(sourceModel()->data(right).toString().isEmpty())
        {
            //qDebug()<<"returning true ..."<<left.row()<<sourceModel()->data(left).toString().isEmpty()<<right.row()<<sourceModel()->data(right).toString().isEmpty();
            return true;
        }
        else
            return sourceModel()->data(left).toString().compare(sourceModel()->data(right).toString(), Qt::CaseInsensitive) < 0;
    }

    return true;
}

QVariant FilterProxy::getData(int id, int column)
{
    for(int i = 0; i < rowCount(); ++i)
    {
        if(data(index(i, ID)).toInt() == id)
            return data(index(i, column));
    }

    return QVariant();
}

QModelIndex FilterProxy::getIndex(int id)
{
    for(int i = 0; i < rowCount(); ++i)
    {
        if(data(index(i, ID)).toInt() == id)
            return index(i, ID);
    }

    return QModelIndex();
}

int FilterProxy::getId(int column, QVariant value)
{
    QVariant defaultValue = "Misc";
    int defaultId = 0;

    for(int i = 0; i < rowCount(); ++i)
    {
        if(data(index(i, column)) == value)
            return data(index(i, ID)).toInt();

        if(data(index(i, NAME)) == defaultValue)
            defaultId = data(index(i, ID)).toInt();
    }

    return defaultId;

}

