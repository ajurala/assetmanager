#include "assetmanager.h"
#include "ui_assetmanager.h"
#include "ui_newinformation.h"
#include "ui_chartoptions.h"

#include <QMessageBox>

extern QString headerInfo[];

AssetManager::AssetManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AssetManager),
    ni(new Ui::NewInformation),
    niDialog(new QDialog(this)),
    co(new Ui::ChartOptions),
    coDialog(new QDialog(this))
{
    hideRisk = false;
    state = NORMAL;

    ui->setupUi(this);
    ni->setupUi(niDialog);
    co->setupUi(coDialog);

    /* Unhide the Tab Close Option for the first tab */
    QTabBar *tabBar = ui->tabWidget->findChild<QTabBar *>();
    tabBar->setTabButton(0, QTabBar::RightSide, 0);

    checkBox = new QCheckBox;
    checkBox->setTristate(true);
    checkBox->setText("Normal View");

    chartCheckBox = new QCheckBox;
    chartCheckBox->setTristate(true);
    chartCheckBox->setText("Pie Chart (2D)");

    legendCheckBox = new QCheckBox;
    legendCheckBox->setText("Round Legend");


    ui->mainToolBar->addWidget(checkBox);
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addWidget(chartCheckBox);
    ui->mainToolBar->addWidget(legendCheckBox);

    atm = new AssetTreeModel("assetManager.db");

    updateTreeView();

    //Populate the risk combo
    FilterProxy *proxy = ((FilterProxy *)atm->riskProxy.data());
    ni->riskComboBox->setModel(proxy);

    proxy = (FilterProxy *)atm->userAccountProxy.data();
    ni->userAccountComboBox->setModel(proxy);

    proxy = (FilterProxy *)atm->categoryProxy.data();
    ni->categoryComboBox->setModel(proxy);


    proxy = (FilterProxy *)atm->instrumentProxy.data();
    ni->instrumentComboBox->setModel(proxy);

    connect(ui->actionNew_User_Account, SIGNAL(triggered()), this, SLOT(newUserAccount()));
    connect(ui->actionCreate_New_Category, SIGNAL(triggered()), this, SLOT(newCategory()));
    connect(ui->actionCreate_New_Instrument, SIGNAL(triggered()), this, SLOT(newInstrument()));
    connect(ui->actionNew_Transaction, SIGNAL(triggered()), this, SLOT(newTransaction()));
    connect(ui->actionNew_Chart, SIGNAL(triggered()), this, SLOT(newChart()));

    connect(ni->instrumentComboBox, SIGNAL(currentIndexChanged(int)), SLOT(instrumentSelected(int)));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveDatabase()));

    connect(ui->actionInsert_New, SIGNAL(triggered()), this, SLOT(insertNew()));
    connect(ui->actionRemove_Row, SIGNAL(triggered()), this, SLOT(removeRow()));
    connect(ui->actionModify, SIGNAL(triggered()), this, SLOT(modifyRow()));

    connect(ui->actionHideRisk, SIGNAL(toggled(bool)), this, SLOT(hideRiskChanged(bool)));
    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(viewStateChanged(int)));
    connect(chartCheckBox, SIGNAL(stateChanged(int)), this, SLOT(chartTypeChanged(int)));
    connect(legendCheckBox, SIGNAL(stateChanged(int)), this, SLOT(legendChanged(int)));


    connect(ui->treeView->header(), SIGNAL(sectionResized ( int, int, int)), this, SLOT(sectionResized(int,int,int)));
    connect(ui->treeView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(doubleClicked(QModelIndex)));
    connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(tabCloseRequested(int)));

    connect(co->selectAllUserAccountsCheckBox, SIGNAL(clicked(bool)), this, SLOT(selectAllUserAccount(bool)));
    connect(co->selecatAllCategoryCheckBox, SIGNAL(clicked(bool)), this, SLOT(selectAllCategory(bool)));
    connect(co->selectAllInstrumentCheckBox, SIGNAL(clicked(bool)), this, SLOT(selectAllInstrument(bool)));
    connect(co->selectAllRiskCheckBox, SIGNAL(clicked(bool)), this, SLOT(selectAllRisk(bool)));

    DoubleValueColumnDelegate *dvcd = new DoubleValueColumnDelegate;
    DateColumnDelegate *dcd = new DateColumnDelegate;
    TextColumnDelegate *tcd = new TextColumnDelegate;

    ui->treeView->setItemDelegateForColumn(NAME, tcd);
    ui->treeView->setItemDelegateForColumn(DESCRIPTION, tcd);

    ui->treeView->setItemDelegateForColumn(UNITS, dvcd);
    ui->treeView->setItemDelegateForColumn(PRICE_PER_UNIT, dvcd);
    ui->treeView->setItemDelegateForColumn(RATE_OF_INTEREST, dvcd);

    ui->treeView->setItemDelegateForColumn(BOUGHT_DATE, dcd);
    ui->treeView->setItemDelegateForColumn(END_DATE, dcd);

    readSettings();

    ui->tabWidget->setCurrentIndex(0);
}

AssetManager::~AssetManager()
{
    delete co;
    delete ni;
    delete ui;
}

void AssetManager::readSettings()
{
    QSettings *settings;

    if(QFile::exists("AssetManager.settings"))
        settings = new QSettings("AssetManager.settings", QSettings::IniFormat);
    else
        settings = new QSettings("PumaSoft", "AssetManager");

    int chartCount = settings->beginReadArray("Charts");
    for(int i = 0; i < chartCount; ++i)
    {
        settings->setArrayIndex(i);

        QString name = settings->value("ChartName").toString();
        QString toolTipText = settings->value("ChartToolTip").toString();
        QList<Filter> filterList;

        int filterListCount = settings->beginReadArray("ChartFilterList");
        for(int j = 0; j < filterListCount; ++j)
        {
            settings->setArrayIndex(j);

            Filter filter;
            filter.colId = settings->value("colId").toInt();

            int filterIdsCount = settings->beginReadArray("ChartFilterIdsList");
            for(int k = 0; k < filterIdsCount; ++k)
            {
                settings->setArrayIndex(k);

                filter.filterIds.append(settings->value("filterId").toInt());
            }
            settings->endArray();

            filterList.append(filter);
        }
        settings->endArray();

        //Create the Proxy and insert the tab
        FilterProxy *gproxy = 0;
        int groupingColumn = settings->value("GroupingColumn").toInt();

        if(groupingColumn == USER_ACCOUNT)
            gproxy = (FilterProxy *)atm->userAccountProxy.data();
        else if(groupingColumn == CATEGORY)
            gproxy = (FilterProxy *)atm->categoryProxy.data();
        else if(groupingColumn == RISK)
            gproxy = (FilterProxy *)atm->riskProxy.data();
        else if(groupingColumn == INSTRUMENT)
            gproxy = (FilterProxy *)atm->instrumentProxy.data();
        else if(groupingColumn == ID)
            gproxy = (FilterProxy *)atm->transactionsProxy.data();

        FilterProxy *dproxy = new FilterProxy(filterList, TRANSACTIONS_TYPE, atm);
        int dataColumn = settings->value("DataColumn").toInt();

        dproxy->setSourceModel(atm->transactionsModel);

        AssetFilter *afilter = new AssetFilter(dproxy, dataColumn, gproxy, groupingColumn);

        int index = ui->tabWidget->addTab(afilter->getTab(), name);

        ui->tabWidget->setTabToolTip(index, toolTipText);

        if( assetFilterList.count() != index - 1 )
        {
            qDebug()<<"This should never happen, and hence exiting";
            exit(997); // This should never happen, and hence exiting
        }

        assetFilterList.append(afilter);

        setChartType(afilter, chartCheckBox->checkState());
    }
    settings->endArray();

    restoreGeometry(settings->value("geometry").toByteArray());
    restoreState(settings->value("windowState").toByteArray());

    coDialog->restoreGeometry(settings->value("chartOptionsGeometry").toByteArray());

    checkBox->setCheckState((Qt::CheckState)settings->value("viewState").toInt());
    ui->actionHideRisk->setChecked(settings->value("hideRisk").toBool());
    chartCheckBox->setCheckState((Qt::CheckState)settings->value("chartType").toInt());
    legendCheckBox->setCheckState((Qt::CheckState)settings->value("legend").toInt());

    int columnSize = settings->beginReadArray("ColumnWidth");
    //qDebug()<<"reading";
    for(int i = 0; i < columnSize; ++i)
    {
        settings->setArrayIndex(i);

        columnWidth[i] = settings->value("width").toInt();

        //qDebug()<<i<<columnWidth[i];

        ui->treeView->setColumnWidth(i, columnWidth[i]);
    }
    settings->endArray();

    delete settings;
}

void AssetManager::closeEvent(QCloseEvent *event)
{
    //qDebug()<<"atm->dirtyState()"<<atm->dirtyState();
    if(atm->dirtyState())
    {
        if(QMessageBox::Yes == QMessageBox::critical(this, "Save",
                                                     "Unsaved Data !!!\n\nDo you want to save?",
                                                    QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
        {
            saveDatabase();
        }
    }

    //QSettings settings("PumaSoft", "AssetManager");
    QSettings settings("AssetManager.settings", QSettings::IniFormat);

    settings.clear();
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());

    settings.setValue("chartOptionsGeometry", coDialog->saveGeometry());


    settings.setValue("hideRisk", ui->actionHideRisk->isChecked());
    settings.setValue("viewState", checkBox->checkState());
    settings.setValue("chartType", chartCheckBox->checkState());
    settings.setValue("legend", legendCheckBox->checkState());

    /* Save the Charts too */

    settings.beginWriteArray("Charts");
    for(int i = 0; i < assetFilterList.count(); ++i)
    {
        settings.setArrayIndex(i);
        FilterProxy *proxy = assetFilterList[i]->getProxy();
        QList<Filter> filterList = proxy->filterList;

        settings.setValue("ChartName", ui->tabWidget->tabText(i + 1));
        settings.setValue("ChartToolTip", ui->tabWidget->tabToolTip(i + 1));

        settings.setValue("GroupingColumn", assetFilterList[i]->getGroupingColumn());
        settings.setValue("DataColumn", assetFilterList[i]->getDataColumn());

        settings.beginWriteArray("ChartFilterList");
        for(int j = 0; j < filterList.count(); ++j)
        {
            settings.setArrayIndex(j);

            settings.setValue("colId", filterList[j].colId);

            settings.beginWriteArray("ChartFilterIdsList");
            for(int k = 0; k < filterList[j].filterIds.count(); ++k)
            {
                settings.setArrayIndex(k);

                settings.setValue("filterId", filterList[j].filterIds[k]);
            }
            settings.endArray();
        }
        settings.endArray();
    }
    settings.endArray();

    /* Save the column width */
    settings.beginWriteArray("ColumnWidth");

    for(int i = 0; i < ui->treeView->header()->count(); ++i)
    {
        settings.setArrayIndex(i);
        //qDebug()<<i<<columnWidth[i];
        settings.setValue("width", columnWidth[i]);
    }
    settings.endArray();
    //qDebug()<<settings.allKeys();
    QMainWindow::closeEvent(event);
}


void AssetManager::updateTreeView()
{
    atm->sortAll();

    ui->treeView->setModel(atm);

    ui->treeView->setColumnHidden(ID, true);
    ui->treeView->setColumnHidden(INSTRUMENT_RISK, true);
    ui->treeView->setColumnHidden(INSTRUMENT_ROI, true);
    ui->treeView->setColumnHidden(INSTRUMENT_CURRENT_PPU, true);
    ui->treeView->setColumnHidden(USER_ACCOUNT, true);
    ui->treeView->setColumnHidden(CATEGORY, true);
    ui->treeView->setColumnHidden(RISK, true);
    ui->treeView->setColumnHidden(HISTORY, true);

    for(int i = 0; i < SCHEMA_END; ++i)
    {
        if(headerInfo[i].contains("RESERVED"))
            ui->treeView->setColumnHidden(i, true);
    }

    ui->treeView->setExpandsOnDoubleClick(false);
    expandAll();

    //resizeColumnsToContents();
    for(int i = 0; i < ui->treeView->header()->count(); ++i)
    {
        ui->treeView->setColumnWidth(i, columnWidth[i]);
    }

    ui->treeView->header()->setHighlightSections(true);
    ui->treeView->header()->setDefaultAlignment(Qt::AlignCenter);
}

void AssetManager::expandAll()
{
    ui->treeView->expandAll();

    //Do not expand the users with no data;
    QModelIndex rootIndex = atm->getIndex(0,0, QModelIndex(), true);

    /** TODO - AJAY - Can create a function in assettreemodel to check for hasLeafTransactions, currently checking directly ... **/
    for(int i = 0; i < atm->rootRow.children.count(); ++i)
    {
        if(!atm->rootRow.children[i].hasLeafTransactions)
            ui->treeView->collapse(atm->index(i, 0, rootIndex));
    }

    /*for(int i = 0; i < atm->rowCount(rootIndex); ++i)
    {
        if(atm->data(atm->index(i, TOTAL_INVESTMENT, rootIndex)).toDouble() <= 0)
        {
            ui->treeView->collapse(atm->index(i, 0, rootIndex));
        }
    }*/
}

void AssetManager::resizeColumnsToContents()
{
    for(int i = 0; i < atm->transactionsModel->columnCount(); ++i)
    {
        ui->treeView->resizeColumnToContents(i);
    }
}

void AssetManager::saveDatabase()
{
    qDebug()<<"Save All";

    //ui->treeView->setModel(0);
    atm->beginResetModel();

    qDebug()<<atm->userAccountModel->submitAll();
    qDebug()<<atm->userAccountModel->lastError();

    qDebug()<<atm->categoryModel->submitAll();
    qDebug()<<atm->categoryModel->lastError();

    qDebug()<<atm->instrumentModel->submitAll();
    qDebug()<<atm->instrumentModel->lastError();

    qDebug()<<atm->transactionsModel->submitAll();
    qDebug()<<atm->transactionsModel->lastError();

    QSqlDatabase db = atm->userAccountModel->database();
    qDebug()<<"Committing";
    qDebug()<<db.commit();
    qDebug()<<db.lastError().text();

    qDebug()<<"Starting a new transaction ...";
    qDebug()<<db.transaction();
    qDebug()<<db.lastError().text();

    atm->endResetModel();

    atm->resetDirtyState();
    expandAll();
    //updateTreeView();
}

void AssetManager::sectionResized ( int logicalIndex, int oldSize, int newSize )
{
    //qDebug()<<"newSize"<<logicalIndex<<newSize;
    columnWidth[logicalIndex] = newSize;
}

void AssetManager::doubleClicked( const QModelIndex &idx )
{
    if(idx.isValid())
    {
        RowType type = atm->indexType(idx);
        int index = 0;
        QString name;
        QString toolTipString;
        AssetFilter *afilter;

        //Show the chart accordingly ...
        if(type == ROOT)
        {
            Filter filter;
            filter.colId = USER_ACCOUNT;

            QList<Filter> userAccountFilter;
            userAccountFilter.append(filter);
            FilterProxy *dproxy = new FilterProxy(userAccountFilter, TRANSACTIONS_TYPE, atm);
            dproxy->setSourceModel(atm->transactionsModel);

            FilterProxy *gproxy = (FilterProxy *)atm->userAccountProxy.data();
            afilter = new AssetFilter(dproxy , TOTAL_INVESTMENT, gproxy, USER_ACCOUNT);

            name = atm->data(atm->index(idx.row(), NAME, idx.parent())).toString();
            toolTipString = name+" - ROOT";

        }
        else if(type == USERACCOUNT_TYPE)
        {
            Filter filter;
            filter.colId = USER_ACCOUNT;
            filter.filterIds.append(atm->data(atm->index(idx.row(), ID, idx.parent())).toInt());

            QList<Filter> userAccountFilter;
            userAccountFilter.append(filter);
            FilterProxy *dproxy = new FilterProxy(userAccountFilter, TRANSACTIONS_TYPE, atm);
            dproxy->setSourceModel(atm->transactionsModel);

            FilterProxy *gproxy = (FilterProxy *)atm->categoryProxy.data();
            afilter = new AssetFilter(dproxy , TOTAL_INVESTMENT, gproxy, CATEGORY);

            name = atm->data(atm->index(idx.row(), NAME, idx.parent())).toString();
            toolTipString = name+" - USER ACCOUNT";
        }
        else if(type == CATEGORY_TYPE)
        {
            Filter filter;
            filter.colId = USER_ACCOUNT;
            filter.filterIds.append(atm->data(atm->index(idx.parent().row(), ID, idx.parent().parent())).toInt());

            QList<Filter> categoryFilter;
            categoryFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = CATEGORY;
            filter.filterIds.append(atm->data(atm->index(idx.row(), ID, idx.parent())).toInt());
            categoryFilter.append(filter);

            FilterProxy *dproxy = new FilterProxy(categoryFilter, TRANSACTIONS_TYPE, atm);
            dproxy->setSourceModel(atm->transactionsModel);

            int groupingColumn = RISK;
            FilterProxy *gproxy = 0;

            if(state == NORMAL && hideRisk == true)
            {
                groupingColumn = ID;
                gproxy = (FilterProxy *)atm->transactionsProxy.data();
            }
            else
            {
                groupingColumn = RISK;
                gproxy = (FilterProxy *)atm->riskProxy.data();
            }
            afilter = new AssetFilter(dproxy , TOTAL_INVESTMENT, gproxy, groupingColumn);

            name = atm->data(atm->index(idx.row(), NAME, idx.parent())).toString();
            toolTipString = name+" - CATEGORY";
        }
        else if(type == RISK_TYPE)
        {
            QList<Filter> riskFilter;

            Filter filter;
            filter.colId = USER_ACCOUNT;
            filter.filterIds.append(atm->data(atm->index(idx.parent().parent().row(), ID, idx.parent().parent().parent())).toInt());
            riskFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = CATEGORY;
            filter.filterIds.append(atm->data(atm->index(idx.parent().row(), ID, idx.parent().parent())).toInt());

            riskFilter.append(filter);

            filter.filterIds.clear();
            filter.colId = RISK;
            filter.filterIds.append(atm->data(atm->index(idx.row(), ID, idx.parent())).toInt());
            riskFilter.append(filter);

            FilterProxy *dproxy = new FilterProxy(riskFilter, TRANSACTIONS_TYPE, atm);
            dproxy->setSourceModel(atm->transactionsModel);

            FilterProxy *gproxy = (FilterProxy *)atm->transactionsProxy.data();
            afilter = new AssetFilter(dproxy , TOTAL_INVESTMENT, gproxy, ID);

            name = atm->data(atm->index(idx.row(), NAME, idx.parent())).toString();
            toolTipString = name+" - RISK";
        }

        if(!name.isEmpty())
        {
            int i = 0;
            for(i = 1; i < ui->tabWidget->count(); ++i)
            {
                if(ui->tabWidget->tabToolTip(i) == toolTipString)
                {
                    index = i;
                    break;
                }
            }

            if(i == ui->tabWidget->count())
            {
                index = ui->tabWidget->addTab(afilter->getTab(), name);
                ui->tabWidget->setTabToolTip(index, toolTipString);

                if( assetFilterList.count() != index - 1 )
                {
                    qDebug()<<"This should never happen, and hence exiting";
                    exit(998); // This should never happen, and hence exiting
                }

                assetFilterList.append(afilter);

                setChartType(afilter, chartCheckBox->checkState());
            }
            else
            {
                //Tab already exists ... so forget it ... maybe this can be improved before even creating the filters ...
                delete afilter->getTab();
            }
            ui->tabWidget->setCurrentIndex(index);
        }
    }
}

void AssetManager::selectAllUserAccount(bool checked)
{
    for(int i = 0; i < userAccountsCheckBoxList.count(); ++i)
        userAccountsCheckBoxList[i]->setChecked(checked);
}

void AssetManager::selectAllCategory(bool checked)
{
    for(int i = 0; i < categoryCheckBoxList.count(); ++i)
        categoryCheckBoxList[i]->setChecked(checked);
}

void AssetManager::selectAllInstrument(bool checked)
{
    for(int i = 0; i < instrumentCheckBoxList.count(); ++i)
        instrumentCheckBoxList[i]->setChecked(checked);
}

void AssetManager::selectAllRisk(bool checked)
{
    for(int i = 0; i < riskCheckBoxList.count(); ++i)
        riskCheckBoxList[i]->setChecked(checked);
}

void AssetManager::newChart()
{
    static int chartCount = 1;
    if(state == MINIMAL)
    {
        co->categoryGroupBox->hide();
        co->riskGroupBox->hide();

        co->categoryRadioButton->hide();
        co->riskRadioButton->hide();
    }
    else
    {
        co->categoryGroupBox->show();
        co->riskGroupBox->show();

        co->categoryRadioButton->show();
        co->riskRadioButton->show();
    }

    FilterProxy *proxy = (FilterProxy *)atm->userAccountProxy.data();
    for(int i = 0; i < proxy->rowCount(); ++i)
    {
        QCheckBox *choiceCheckBox = new QCheckBox(proxy->data(proxy->index(i, NAME)).toString(), co->userAccountsGroupBox);
        userAccountsCheckBoxList.append(choiceCheckBox);
        co->userAccountScrollAreaVerticalLayout->addWidget(choiceCheckBox);
    }

    proxy = (FilterProxy *)atm->categoryProxy.data();
    for(int i = 0; i < proxy->rowCount(); ++i)
    {
        QCheckBox *choiceCheckBox = new QCheckBox(proxy->data(proxy->index(i, NAME)).toString(), co->categoryGroupBox);
        categoryCheckBoxList.append(choiceCheckBox);
        co->categoryScrollAreaVerticalLayout->addWidget(choiceCheckBox);
    }

    proxy = (FilterProxy *)atm->instrumentProxy.data();
    for(int i = 0; i < proxy->rowCount(); ++i)
    {
        QCheckBox *choiceCheckBox = new QCheckBox(proxy->data(proxy->index(i, NAME)).toString(), co->instrumentGroupBox);
        instrumentCheckBoxList.append(choiceCheckBox);
        co->instrumentScrollAreaVerticalLayout->addWidget(choiceCheckBox);
        //co->instrumentScrollArea->addScrollBarWidget();
    }

    proxy = (FilterProxy *)atm->riskProxy.data();
    for(int i = 0; i < proxy->rowCount(); ++i)
    {
        QCheckBox *choiceCheckBox = new QCheckBox(proxy->data(proxy->index(i, NAME)).toString(), co->riskGroupBox);
        riskCheckBoxList.append(choiceCheckBox);
        co->riskScrollAreaVerticalLayout->addWidget(choiceCheckBox);
    }

    //co->instrumentRadioButton->setChecked(true);    

    int ret = QDialog::Accepted;
    bool popupAccepted = false;
    QString name;

    while(popupAccepted == false)
    {
        ret = coDialog->exec();

        if(ret == QDialog::Accepted)
        {
            name = co->chartNameLineEdit->text().trimmed();
            if(name.isEmpty())
            {
                if(QMessageBox::Yes == QMessageBox::information(this, "Empty Chart Name", "Chart name is empty. Do you want to continue with a default name?",
                                         QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                {
                    popupAccepted = true;
                }
            }
            else
            {
                //Search for the same name ...
                int i = 1;
                for(i = 1; i < ui->tabWidget->count(); ++i)
                {
                    if(ui->tabWidget->tabText(i) == name)
                    {
                        if(QMessageBox::Yes == QMessageBox::information(this, "Same Chart Name", "Chart name matches with existing chart name. Do you want to continue with the same chart name?",
                                                 QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                        {
                            popupAccepted = true;
                        }

                        break;
                    }
                }

                if(i == ui->tabWidget->count())
                    popupAccepted = true;
            }
        }
        else
            break;
    }

    if(ret == QDialog::Accepted)
    {
        QString toolTipString;
        QList<Filter> filterList;

        Filter filter;
        filter.colId = USER_ACCOUNT;

        proxy = (FilterProxy *)atm->userAccountProxy.data();
        toolTipString += "User Account";
        for(int i = 0; i < userAccountsCheckBoxList.count(); ++i)
        {
            if(userAccountsCheckBoxList[i]->isChecked())
            {
                filter.filterIds.append(proxy->data(proxy->index(i, ID)).toInt());
                toolTipString += "-"+userAccountsCheckBoxList[i]->text();
            }
        }
        filterList.append(filter);

        filter.filterIds.clear();
        filter.colId = CATEGORY;
        proxy = (FilterProxy *)atm->categoryProxy.data();
        toolTipString += " Category";
        for(int i = 0; i < categoryCheckBoxList.count(); ++i)
        {
            if(categoryCheckBoxList[i]->isChecked())
            {
                filter.filterIds.append(proxy->data(proxy->index(i, ID)).toInt());
                toolTipString += "-"+categoryCheckBoxList[i]->text();
            }
        }
        filterList.append(filter);

        filter.filterIds.clear();
        filter.colId = INSTRUMENT;
        proxy = (FilterProxy *)atm->instrumentProxy.data();
        toolTipString += " Instrument";
        for(int i = 0; i < instrumentCheckBoxList.count(); ++i)
        {
            if(instrumentCheckBoxList[i]->isChecked())
            {
                filter.filterIds.append(proxy->data(proxy->index(i, ID)).toInt());
                toolTipString += "-"+instrumentCheckBoxList[i]->text();
            }
        }
        filterList.append(filter);

        filter.filterIds.clear();
        filter.colId = RISK;
        proxy = (FilterProxy *)atm->riskProxy.data();
        toolTipString += " Risk";
        for(int i = 0; i < riskCheckBoxList.count(); ++i)
        {
            if(riskCheckBoxList[i]->isChecked())
            {
                filter.filterIds.append(proxy->data(proxy->index(i, ID)).toInt());
                toolTipString += "-"+riskCheckBoxList[i]->text();
            }
        }
        filterList.append(filter);

        FilterProxy *gProxy = 0;
        int groupingColumn = INSTRUMENT;
        toolTipString += " Grouping";
        if(co->userAccountsRadioButton->isChecked())
        {
            toolTipString += "-"+co->userAccountsRadioButton->text();
            gProxy = (FilterProxy *)atm->userAccountProxy.data();
            groupingColumn = USER_ACCOUNT;
        }
        else if(co->categoryRadioButton->isChecked())
        {
            toolTipString += "-"+co->categoryRadioButton->text();
            gProxy = (FilterProxy *)atm->categoryProxy.data();
            groupingColumn = CATEGORY;
        }
        else if(co->instrumentRadioButton->isChecked())
        {
            toolTipString += "-"+co->instrumentRadioButton->text();
            gProxy = (FilterProxy *)atm->instrumentProxy.data();
            groupingColumn = INSTRUMENT;
        }
        else if(co->riskRadioButton->isChecked())
        {
            toolTipString += "-"+co->riskRadioButton->text();
            gProxy = (FilterProxy *)atm->riskProxy.data();
            groupingColumn = RISK;
        }
        else if(co->transactionsRadioButton->isChecked())
        {
            toolTipString += "-"+co->transactionsRadioButton->text();
            gProxy = (FilterProxy *)atm->transactionsProxy.data();
            groupingColumn = ID; // basically transaction itself :D
        }

        int dataColumn = TOTAL_INVESTMENT;
        toolTipString += " Data";
        if(co->totalInvestmentRadioButton->isChecked())
        {
            toolTipString += "-"+co->totalInvestmentRadioButton->text();
            dataColumn = TOTAL_INVESTMENT;
        }
        else if(co->currentInvestmentRadioButton->isChecked())
        {
            toolTipString += "-"+co->currentInvestmentRadioButton->text();
            dataColumn = CURRENT_TOTAL;
        }
        else if(co->currentInvestmentROIRadioButton->isChecked())
        {
            toolTipString += "-"+co->currentInvestmentROIRadioButton->text();
            dataColumn = CURRENT_TOTAL_ROI;
        }

        if(!toolTipString.isEmpty())
        {
            int i = 0;
            int index = 0;
            for(i = 1; i < ui->tabWidget->count(); ++i)
            {
                if(ui->tabWidget->tabToolTip(i) == toolTipString)
                {
                    index = i;
                    break;
                }
            }

            if(i == ui->tabWidget->count())
            {
                FilterProxy *dProxy = new FilterProxy(filterList, TRANSACTIONS_TYPE, atm);
                dProxy->setSourceModel(atm->transactionsModel);

                name = name.isEmpty() ? "Multi "+QString::number(chartCount++) : name;
                AssetFilter *afilter = new AssetFilter(dProxy , dataColumn, gProxy, groupingColumn);

                index = ui->tabWidget->addTab(afilter->getTab(), name);
                ui->tabWidget->setTabToolTip(index, toolTipString);

                if( assetFilterList.count() != index - 1 )
                {
                    qDebug()<<"This should never happen, and hence exiting";
                    exit(999); // This should never happen, and hence exiting
                }

                assetFilterList.append(afilter);

                setChartType(afilter, chartCheckBox->checkState());
            }

            ui->tabWidget->setCurrentIndex(index);
        }

    }

    qDeleteAll(userAccountsCheckBoxList);
    qDeleteAll(categoryCheckBoxList);
    qDeleteAll(instrumentCheckBoxList);
    qDeleteAll(riskCheckBoxList);

    userAccountsCheckBoxList.clear();
    categoryCheckBoxList.clear();
    instrumentCheckBoxList.clear();
    riskCheckBoxList.clear();

    co->selectAllUserAccountsCheckBox->setChecked(false);
    co->selecatAllCategoryCheckBox->setChecked(false);
    co->selectAllInstrumentCheckBox->setChecked(false);
    co->selectAllRiskCheckBox->setChecked(false);
}

void AssetManager::tabCloseRequested(int index)
{
    if(index > 0)
    {
        if(QMessageBox::Yes == QMessageBox::critical(this, "Close Chart",
                                                     "Are you sure you want to close the chart " + ui->tabWidget->tabText(index) + " ?",
                                                    QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
        {
            QWidget *widget = ui->tabWidget->widget(index);
            ui->tabWidget->removeTab(index);

            assetFilterList.removeAt(index - 1);

            delete widget;
        }
    }
}

void AssetManager::instrumentSelected(int index)
{
    //qDebug()<<"instrumentSelected"<<index;
    FilterProxy *proxy = (FilterProxy *)atm->instrumentProxy.data();
    bool ok;
    ni->riskComboBox->setCurrentIndex(proxy->data(proxy->index(index, INSTRUMENT_RISK)).toInt(&ok));

    //qDebug()<<"insSel"<<ok<<proxy->data(proxy->index(index, INSTRUMENT_RISK)).toInt();
}

void AssetManager::insertNew()
{
    QModelIndex idx = ui->treeView->selectionModel()->currentIndex();
    if(idx.isValid())
    {

    RowType type = atm->indexType(idx);

    if(type == TRANSACTIONS_TYPE)
    {
        //int id = atm->getInstrumentId(idx);

        if(idx.column() == INSTRUMENT)
        {
            newInstrument();
        }
        else
        {
            atm->insertRow(idx.row(), idx.parent());
            //atm->setData(atm->getIndex(idx.row(), INSTRUMENT, idx.parent(), true), id);
            expandAll();
        }
    }
    else if(type == USERACCOUNT_TYPE)
    {
        newUserAccount();
    }
    else if(type == CATEGORY_TYPE)
    {
        newCategory();
    }

    //The rest ignore
    }
}

void AssetManager::removeRow()
{
    QModelIndex idx = ui->treeView->selectionModel()->currentIndex();
    if(idx.isValid())
    {
    RowType type = atm->indexType(idx);

    if(type != RISK_TYPE)
    {
        int column = idx.column() == INSTRUMENT ? INSTRUMENT : NAME;
        QString name = atm->data(atm->index(idx.row(), column, idx.parent())).toString();
        if((type != TRANSACTIONS_TYPE || (type == TRANSACTIONS_TYPE && idx.column() == INSTRUMENT))
                &&  name == "Misc")
            QMessageBox::information(this, "Not Allowed", "Default User Account / Classification / Instrument cannot be deleted");
        else
        {
                if(type == TRANSACTIONS_TYPE)
                {
                    if(idx.column() == INSTRUMENT)
                    {
                        if(QMessageBox::Yes == QMessageBox::critical(this, "Delete Instrument",
                                                                     "Are you sure you want to delete?\n\nTransactions under Instrument '" + name + "' will be moved to default Instrument \'Misc\'",
                                                                    QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                            removeInstrument(idx);
                    }
                    else
                    {
                        if(QMessageBox::Yes == QMessageBox::critical(this, "Delete Transaction",
                                                                     "Are you sure you want to delete the transaction '" + name + "'?",
                                                                    QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                            removeTransaction(idx);
                    }
                }
                else if(type == USERACCOUNT_TYPE)
                {
                    if(QMessageBox::Yes == QMessageBox::critical(this, "Delete User Account",
                                                                 "Are you sure you want to delete?\n\nTransactions under User Account '" + name + "' will be moved to default User Account \'Misc\'",
                                                                QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                        removeUserAccount(idx);
                }
                else if(type == CATEGORY_TYPE)
                {
                    if(QMessageBox::Yes == QMessageBox::critical(this, "Delete Category",
                                                                 "Are you sure you want to delete?\n\nTransactions under Classification '" + name + "' will be moved to default Classification \'Misc\'",
                                                                QMessageBox::StandardButtons(QMessageBox::Yes|QMessageBox::No), QMessageBox::No))
                        removeCategory(idx);
                }
            }
    }
    }
}

void AssetManager::modifyRow()
{
    QModelIndex idx = ui->treeView->selectionModel()->currentIndex();
    if(idx.isValid())
    {

    RowType type = atm->indexType(idx);


    if(type != RISK_TYPE)
    {
        int column = idx.column() == INSTRUMENT ? INSTRUMENT : NAME;

        if((type != TRANSACTIONS_TYPE || (type == TRANSACTIONS_TYPE && idx.column() == INSTRUMENT))
                && atm->data(atm->index(idx.row(), column, idx.parent())).toString() == "Misc")
            QMessageBox::information(this, "Not Allowed", "Default User Account / Classification / Instrument cannot be modified");
        else
        {
            if(type == TRANSACTIONS_TYPE)
            {
                if(idx.column() == INSTRUMENT)
                    updateInstrument(idx);
                else
                    updateTransaction(idx);
            }
            else if(type == USERACCOUNT_TYPE)
            {
                updateUserAccount(idx);
            }
            else if(type == CATEGORY_TYPE)
            {
                updateCategory(idx);
            }
        }
    }
    }

}

void AssetManager::viewStateChanged(int viewState)
{
    state = (ModelState)viewState;

    if(state == NORMAL)
    {
        checkBox->setText("Normal View");
        ui->actionHideRisk->setEnabled(true);
    }
    else if(state == MINIMAL)
    {
        checkBox->setText("Minimal View");
        ui->actionHideRisk->setDisabled(true);
    }
    else if(state == FULL)
    {
        checkBox->setText("Full View");
        ui->actionHideRisk->setDisabled(true);
    }

    atm->setState(state);
    atm->sortAll();
    expandAll();
}

void AssetManager::hideRiskChanged(bool state)
{
    hideRisk = state;
    atm->setHideRisk(hideRisk);
    atm->sortAll();
    expandAll();
}

void AssetManager::chartTypeChanged(int viewState)
{
    if(viewState == 0)
    {
        chartCheckBox->setText("Pie Chart (2D)");
    }
    else if(viewState == 1)
    {
        chartCheckBox->setText("Pie Chart (3D)");
    }
    else if(viewState == 2)
    {
        chartCheckBox->setText("Histogram");
    }

    setAllChartType(viewState);
}

void AssetManager::legendChanged(int viewState)
{
    if(viewState == 0)
    {
        legendCheckBox->setText("Round Legend");
    }
    else if(viewState == 2)
    {
        legendCheckBox->setText("Vertical Legend");
    }
    setAllLegend(viewState);
}

void AssetManager::setChartType(AssetFilter *afilter, int state, bool updateLegend)
{
    if(state == 0)
    {
        afilter->setChartType(Nightcharts::Pie);
        legendCheckBox->setEnabled(true);
        if(updateLegend)
            setLegend(afilter, legendCheckBox->checkState(), false);
    }
    else if(state == 1)
    {
        afilter->setChartType(Nightcharts::DPie);
        legendCheckBox->setEnabled(true);
        if(updateLegend)
            setLegend(afilter, legendCheckBox->checkState(), false);
    }
    else if(state == 2)
    {
        afilter->setChartType(Nightcharts::Histogram);
        legendCheckBox->setDisabled(true);
        if(updateLegend)
            setLegend(afilter, Qt::Checked, false);
    }

    if(updateLegend)
        afilter->update();
}

void AssetManager::setLegend(AssetFilter *afilter, int state, bool updateChartType)
{
    if(state == 0)
    {
        afilter->setLegend(Nightcharts::Round);
    }
    else if(state == 2)
    {
        afilter->setLegend(Nightcharts::Vertical);
    }

    if(updateChartType)
    {
        setChartType(afilter, chartCheckBox->checkState(), false);
        afilter->update();
    }
}

void AssetManager::setAllChartType(int chartType)
{
    for(int i = 0; i < assetFilterList.count(); ++i)
    {
        setChartType(assetFilterList[i], chartType);
    }
}

void AssetManager::setAllLegend(int legend)
{
    for(int i = 0; i < assetFilterList.count(); ++i)
    {
        setLegend(assetFilterList[i], legend);
    }
}

void AssetManager::removeUserAccount(QModelIndex &idx)
{
    FilterProxy *proxy = ((FilterProxy *)atm->userAccountProxy.data());
    int previousId = atm->data(atm->index(idx.row(), ID, idx.parent())).toInt();
    int defaultId = proxy->getId(NAME, "Misc");

    atm->beginResetModel();

    for(int i = 0; i <  atm->transactionsModel->rowCount(); ++i)
    {
        if(atm->transactionsModel->data(atm->transactionsModel->index(i, USER_ACCOUNT)).toInt() == previousId)
        {
            atm->transactionsModel->setData(atm->transactionsModel->index(i, USER_ACCOUNT), defaultId);
        }
    }

    atm->removeRow(idx.row(), idx.parent());

    atm->resetDirtyState(true);

    atm->transactionsModel->submitAll();
    atm->userAccountModel->submitAll();

    atm->endResetModel();
    expandAll();
}

void AssetManager::removeCategory(QModelIndex &idx)
{
    FilterProxy *proxy = ((FilterProxy *)atm->categoryProxy.data());
    bool ok;
    int previousId = atm->data(atm->index(idx.row(), ID, idx.parent())).toInt(&ok);
    int defaultId = proxy->getId(NAME, "Misc");

    //qDebug()<<previousId<<previousId<<ok;

    atm->beginResetModel();

    for(int i = 0; i <  atm->transactionsModel->rowCount(); ++i)
    {
        if(atm->transactionsModel->data(atm->transactionsModel->index(i, CATEGORY)).toInt() == previousId)
        {
            atm->transactionsModel->setData(atm->transactionsModel->index(i, CATEGORY), defaultId);
        }
    }

    atm->removeRow(idx.row(), idx.parent());


    atm->resetDirtyState(true);

    atm->transactionsModel->submitAll();
    atm->categoryModel->submitAll();

    atm->endResetModel();
    expandAll();

}

void AssetManager::removeInstrument(QModelIndex &idx)
{
    FilterProxy *proxy = ((FilterProxy *)atm->instrumentProxy.data());
    int previousId = atm->getData(idx, Qt::DisplayRole, true).toInt();
    int defaultId = proxy->getId(NAME, "Misc");

    atm->beginResetModel();

    for(int i = 0; i <  atm->transactionsModel->rowCount(); ++i)
    {
        if(atm->transactionsModel->data(atm->transactionsModel->index(i, INSTRUMENT)).toInt() == previousId)
        {
            atm->transactionsModel->setData(atm->transactionsModel->index(i, INSTRUMENT), defaultId);
        }
    }

    for(int i = 0; i < proxy->rowCount(); ++i)
    {
        if(proxy->data(proxy->index(i, ID)).toInt() == previousId)
        {
            proxy->removeRow(i);
            break;
            //--i; // as the 'i' would increment, but we have removed the current row ...
        }
    }


    atm->resetDirtyState(true);

    atm->transactionsModel->submitAll();
    atm->instrumentModel->submitAll();

    atm->endResetModel();
    expandAll();

}

void AssetManager::removeTransaction(QModelIndex &idx)
{
    atm->removeRow(idx.row(), idx.parent());
    atm->transactionsModel->submitAll();

    atm->sortAll();

    expandAll();
}


void AssetManager::updateUserAccount(QModelIndex &idx)
{
    niDialog->setWindowTitle("Update User Account");
    ni->nameLineEdit->setText(atm->data(atm->index(idx.row(), NAME, idx.parent())).toString());
    ni->descriptionPlainTextEdit->setPlainText(atm->data(atm->index(idx.row(), DESCRIPTION, idx.parent())).toString());

    if(showUserAccountDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);
        atm->beginResetModel();

        //update the db
        //QModelIndex rootIndex = atm->getIndex(0,0, QModelIndex(), true);
        //FilterProxy *proxy = ((FilterProxy *)atm->userAccountProxy.data());
        //atm->insertRow(atm->rowCount(rootIndex), rootIndex);

        //rootIndex = atm->getIndex(0,0, QModelIndex(), true);

        atm->setData(atm->index(idx.row(), DESCRIPTION, idx.parent()), ni->descriptionPlainTextEdit->toPlainText());
        atm->setData(atm->index(idx.row(), NAME, idx.parent()), ni->nameLineEdit->text());

        atm->userAccountModel->submitAll();

        atm->endResetModel();
        expandAll();

        //updateTreeView();
    }
}

void AssetManager::updateCategory(QModelIndex &idx)
{
    niDialog->setWindowTitle("Update Category");
    ni->nameLineEdit->setText(atm->data(atm->index(idx.row(), NAME, idx.parent())).toString());
    ni->descriptionPlainTextEdit->setPlainText(atm->data(atm->index(idx.row(), DESCRIPTION, idx.parent())).toString());

    if(showCategoryDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

        //update the db
        //QModelIndex initialIndex = atm->getIndex(0,0, QModelIndex(), true);
        //QModelIndex rootIndex = atm->getIndex(0,0, initialIndex, true);

        //atm->insertRow(atm->rowCount(rootIndex), rootIndex);

        //initialIndex = atm->getIndex(0,0);
        //rootIndex = atm->getIndex(0,0, initialIndex, true);

        atm->beginResetModel();

        atm->setData(atm->index(idx.row(), DESCRIPTION, idx.parent()), ni->descriptionPlainTextEdit->toPlainText());
        atm->setData(atm->index(idx.row(), NAME, idx.parent()), ni->nameLineEdit->text());

        atm->categoryModel->submitAll();

        //updateTreeView();
        atm->endResetModel();
        expandAll();
    }
}

void AssetManager::updateInstrument(QModelIndex &idx)
{
    niDialog->setWindowTitle("Update Instrument");
    ni->nameLineEdit->setText(atm->data(idx).toString());

    FilterProxy *proxy = ((FilterProxy *)atm->instrumentProxy.data());
    int instrumentId = atm->getInstrumentId(idx);
    QModelIndex ind = proxy->getIndex(instrumentId);
    //qDebug()<<"updateInstrument"<<instrumentId<<ind.isValid()<<proxy->data(proxy->index(ind.row(), INSTRUMENT_RISK)).toInt();

    ni->descriptionPlainTextEdit->setPlainText(proxy->data(proxy->index(ind.row(), DESCRIPTION)).toString());
    ni->rateOfInterestDoubleSpinBox->setValue(proxy->data(proxy->index(ind.row(), INSTRUMENT_ROI)).toDouble());
    ni->riskComboBox->setCurrentIndex(proxy->data(proxy->index(ind.row(), INSTRUMENT_RISK)).toInt());
    ni->currentPriceDoubleSpinBox->setValue(proxy->data(proxy->index(ind.row(), INSTRUMENT_CURRENT_PPU)).toDouble());

    double previousCurrentPPU = proxy->data(proxy->index(ind.row(), INSTRUMENT_CURRENT_PPU)).toDouble();
    int previousRisk = proxy->data(proxy->index(ind.row(), INSTRUMENT_RISK)).toInt();

    if(showInstrumentDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

        //update the db


        //proxy->insertRow(proxy->rowCount());

        atm->beginResetModel();

        proxy->setData(proxy->index(ind.row(), NAME), ni->nameLineEdit->text());
        proxy->setData(proxy->index(ind.row(), DESCRIPTION), ni->descriptionPlainTextEdit->toPlainText());

        //risk roi cppu
        FilterProxy *rproxy = ((FilterProxy *)atm->riskProxy.data());
        proxy->setData(proxy->index(ind.row(),INSTRUMENT_RISK), rproxy->data(rproxy->index(ni->riskComboBox->currentIndex(), ID)));
        proxy->setData(proxy->index(ind.row(), INSTRUMENT_ROI), ni->rateOfInterestDoubleSpinBox->text().toDouble());
        proxy->setData(proxy->index(ind.row(), INSTRUMENT_CURRENT_PPU), ni->currentPriceDoubleSpinBox->text().toDouble());

        double currentPPU = ni->currentPriceDoubleSpinBox->text().toDouble();
        int currentRisk = rproxy->data(rproxy->index(ni->riskComboBox->currentIndex(), ID)).toInt();
        if(previousCurrentPPU != currentPPU
                || previousRisk != currentRisk)
        {
            //Update all the transactions ...
            for(int i = 0; i < atm->transactionsModel->rowCount(); ++i)
            {
                if(atm->transactionsModel->data(atm->transactionsModel->index(i, INSTRUMENT)).toInt() == instrumentId)
                {
                    if(previousCurrentPPU != currentPPU)
                        atm->transactionsModel->setData(atm->transactionsModel->index(i, CURRENT_TOTAL),
                                                    currentPPU * atm->transactionsModel->data(atm->transactionsModel->index(i, UNITS)).toDouble());
                    if(previousRisk != currentRisk)
                        atm->transactionsModel->setData(atm->transactionsModel->index(i, RISK),
                                                        currentRisk);

                }
            }
        }

        atm->resetDirtyState(true);

        atm->instrumentModel->submitAll();
        atm->endResetModel();
        expandAll();

        //updateTreeView();
    }
}

void AssetManager::updateTransaction(QModelIndex &idx)
{
    niDialog->setWindowTitle("Update Transaction");
    ni->nameLineEdit->setText(atm->data(atm->index(idx.row(), NAME, idx.parent())).toString());
    ni->descriptionPlainTextEdit->setPlainText(atm->data(atm->index(idx.row(), DESCRIPTION, idx.parent())).toString());

    FilterProxy *proxy = ((FilterProxy *)atm->instrumentProxy.data());
    int id = atm->getInstrumentId(idx);
    QModelIndex iIndex = proxy->getIndex(id);

    proxy = ((FilterProxy *)atm->userAccountProxy.data());
    bool ok;
    id = atm->data(atm->index(idx.row(), USER_ACCOUNT, idx.parent())).toInt(&ok);
    QModelIndex uIndex = proxy->getIndex(id);

    proxy = ((FilterProxy *)atm->categoryProxy.data());
    id = atm->data(atm->index(idx.row(), CATEGORY, idx.parent())).toInt();
    QModelIndex cIndex = proxy->getIndex(id);

    ni->userAccountComboBox->setCurrentIndex(uIndex.row());
    ni->categoryComboBox->setCurrentIndex(cIndex.row());
    ni->instrumentComboBox->setCurrentIndex(iIndex.row());
    ni->riskComboBox->setCurrentIndex(atm->data(atm->index(idx.row(), RISK, idx.parent())).toInt());
    ni->boughtDateEdit->setDate(atm->data(atm->index(idx.row(), BOUGHT_DATE, idx.parent())).toDate());
    ni->endDateEdit->setDate(atm->data(atm->index(idx.row(), END_DATE, idx.parent())).toDate());
    ni->unitsDoubleSpinBox->setValue(atm->data(atm->index(idx.row(), UNITS, idx.parent())).toDouble());
    ni->pricePerUnitDoubleSpinBox->setValue(atm->data(atm->index(idx.row(), PRICE_PER_UNIT, idx.parent())).toDouble());
    ni->rateOfInterestDoubleSpinBox->setValue(atm->data(atm->index(idx.row(), RATE_OF_INTEREST, idx.parent())).toDouble());

    if(showTransactionDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

//        QModelIndex initialIndex = atm->getIndex(0,0, QModelIndex(), true);
//        QModelIndex userAccountIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);
//        QModelIndex categoryIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), 0, userAccountIndex, true);
//        QModelIndex rootIndex = atm->getIndex(ni->riskComboBox->currentIndex(),0, categoryIndex, true);

//        atm->insertRow(atm->rowCount(rootIndex), rootIndex);

//        initialIndex = atm->getIndex(0,0, QModelIndex(), true);
//        userAccountIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);
//        categoryIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), 0, userAccountIndex, true);
//        rootIndex = atm->getIndex(ni->riskComboBox->currentIndex(),0, categoryIndex, true);

//        QModelIndex userAccountIDIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), ID, initialIndex, true);
//        QModelIndex categoryIDIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), ID, userAccountIndex, true);

        atm->beginResetModel();

        FilterProxy *iproxy = (FilterProxy *)atm->instrumentProxy.data();
        FilterProxy *uproxy = ((FilterProxy *)atm->userAccountProxy.data());
        FilterProxy *cproxy = ((FilterProxy *)atm->categoryProxy.data());

        int row = idx.row();
        QModelIndex rootIndex = idx.parent();

        atm->setData(atm->index(row, NAME, rootIndex), ni->nameLineEdit->text());
        atm->setData(atm->index(row, DESCRIPTION, rootIndex), ni->descriptionPlainTextEdit->toPlainText());

        /** TODO - These on setting can cause some serious mismatch ... Currently works fine **/
        atm->setData(atm->index(row, USER_ACCOUNT, rootIndex), uproxy->data(uproxy->index(ni->userAccountComboBox->currentIndex(), ID)));
        atm->setData(atm->index(row, CATEGORY, rootIndex), cproxy->data(cproxy->index(ni->categoryComboBox->currentIndex(), ID)));
        atm->setData(atm->index(row, RISK, rootIndex),  ni->riskComboBox->currentIndex());

        atm->setData(atm->index(row, INSTRUMENT, rootIndex), iproxy->data(iproxy->index(ni->instrumentComboBox->currentIndex(), ID)));

        atm->setData(atm->index(row, BOUGHT_DATE, rootIndex), ni->boughtDateEdit->date());
        atm->setData(atm->index(row, END_DATE, rootIndex), ni->endDateEdit->date());

        atm->setData(atm->index(row, RATE_OF_INTEREST, rootIndex), ni->rateOfInterestDoubleSpinBox->value());
        atm->setData(atm->index(row, PRICE_PER_UNIT, rootIndex), ni->pricePerUnitDoubleSpinBox->value());
        atm->setData(atm->index(row, UNITS, rootIndex), ni->unitsDoubleSpinBox->value());

        atm->transactionsModel->submitAll();

        atm->endResetModel();
        expandAll();

        //updateTreeView();
    }
}


int AssetManager::showUserAccountDialog()
{
    ni->riskGroupBox->hide();
    ni->transactionGroupBox_1->hide();
    ni->transactionGroupBox_2->hide();
    ni->currentPriceGroupBox->hide();
    ni->rateGroupBox->hide();

    ni->basicInformation->show();

    niDialog->resize(niDialog->width(), niDialog->minimumHeight());
    ni->nameLineEdit->setFocus();

    dialogType = USERACCOUNT_TYPE;

    QString previousName = ni->nameLineEdit->text();
    bool invalid = true;
    int ret = QDialog::Rejected;
    while(invalid)
    {
        ret = niDialog->exec();

        if(ret == QDialog::Rejected) invalid = false;
        else if(!ni->nameLineEdit->text().isEmpty())
        {
            //Check whether valid instrument name;
            FilterProxy *proxy = ((FilterProxy *)atm->userAccountProxy.data());
            int i = 0;
            for(i = 0; i < proxy->rowCount(); ++i)
            {
                if(proxy->data(proxy->index(i, NAME)).toString() == ni->nameLineEdit->text()
                        && previousName != ni->nameLineEdit->text())
                    break;
            }

            if(i == proxy->rowCount())
            {
                invalid = false;
            }
        }

        if(invalid)
        {
            QMessageBox::critical(this, "Invalid Name", "Please Enter a Unique Name for this user");
        }
    }

    return ret;
}

void AssetManager::newUserAccount()
{
    niDialog->setWindowTitle("New User Account");
    ni->nameLineEdit->setText("");
    ni->descriptionPlainTextEdit->setPlainText("");

    if(showUserAccountDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

        //update the db
        QModelIndex rootIndex = atm->getIndex(0,0, QModelIndex(), true);
        //FilterProxy *proxy = ((FilterProxy *)atm->userAccountProxy.data());
        atm->insertRow(atm->rowCount(rootIndex), rootIndex);

        rootIndex = atm->getIndex(0,0, QModelIndex(), true);

        atm->setData(atm->getIndex(atm->rowCount(rootIndex) - 1, NAME, rootIndex, true), ni->nameLineEdit->text());
        atm->setData(atm->getIndex(atm->rowCount(rootIndex) - 1, DESCRIPTION, rootIndex, true), ni->descriptionPlainTextEdit->toPlainText());

        atm->userAccountModel->submitAll();

        atm->sortAll();

        expandAll();

        //updateTreeView();
    }
}

int AssetManager::showCategoryDialog()
{

    ni->riskGroupBox->hide();
    ni->transactionGroupBox_1->hide();
    ni->transactionGroupBox_2->hide();
    ni->currentPriceGroupBox->hide();
    ni->rateGroupBox->hide();

    ni->basicInformation->show();

    niDialog->resize(niDialog->width(), niDialog->minimumHeight());
    ni->nameLineEdit->setFocus();

    dialogType = CATEGORY_TYPE;

    QString previousName = ni->nameLineEdit->text();
    bool invalid = true;
    int ret = QDialog::Rejected;
    while(invalid)
    {
        ret = niDialog->exec();

        if(ret == QDialog::Rejected) invalid = false;
        else if(!ni->nameLineEdit->text().isEmpty())
        {
            //Check whether valid category name;
            FilterProxy *proxy = ((FilterProxy *)atm->categoryProxy.data());
            int i = 0;
            for(i = 0; i < proxy->rowCount(); ++i)
            {
                if(proxy->data(proxy->index(i, NAME)).toString() == ni->nameLineEdit->text()
                        && previousName != ni->nameLineEdit->text())
                    break;
            }

            if(i == proxy->rowCount())
            {
                invalid = false;
            }
        }

        if(invalid)
        {
            QMessageBox::critical(this, "Invalid Name", "Please Enter a Unique Name for this category");
        }
    }

    return ret;
}

void AssetManager::newCategory()
{
    niDialog->setWindowTitle("New Category");
    ni->nameLineEdit->setText("");
    ni->descriptionPlainTextEdit->setPlainText("");


    if(showCategoryDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);
        //update the db
        if(state == MINIMAL)
        {
            FilterProxy *proxy = (FilterProxy *)atm->categoryProxy.data();

            proxy->insertRow(proxy->rowCount());

            proxy->setData(proxy->index(proxy->rowCount() - 1, NAME), ni->nameLineEdit->text());
            proxy->setData(proxy->index(proxy->rowCount() - 1, DESCRIPTION), ni->descriptionPlainTextEdit->toPlainText());

            atm->resetDirtyState(true);

            atm->categoryModel->submitAll();
        }
        else
        {
            QModelIndex initialIndex = atm->getIndex(0,0, QModelIndex(), true);
            QModelIndex rootIndex = atm->getIndex(0,0, initialIndex, true);

            atm->insertRow(atm->rowCount(rootIndex), rootIndex);

            initialIndex = atm->getIndex(0,0);
            rootIndex = atm->getIndex(0,0, initialIndex, true);

            atm->setData(atm->getIndex(atm->rowCount(rootIndex) - 1, NAME, rootIndex, true), ni->nameLineEdit->text());
            atm->setData(atm->getIndex(atm->rowCount(rootIndex) - 1, DESCRIPTION, rootIndex, true), ni->descriptionPlainTextEdit->toPlainText());

            atm->categoryModel->submitAll();

            atm->sortAll();

            expandAll();
        }

        //updateTreeView();
    }
}

int AssetManager::showInstrumentDialog()
{
    //qDebug()<<ni->riskComboBox->currentIndex();

    ni->transactionGroupBox_1->hide();
    ni->transactionGroupBox_2->hide();

    ni->currentPriceGroupBox->show();
    ni->rateGroupBox->show();

    ni->riskGroupBox->show();
    //qDebug()<<"aft show"<<ni->riskComboBox->currentIndex();

    ni->basicInformation->show();

    ni->riskComboBox->setEnabled(true);
    //qDebug()<<"after enable"<<ni->riskComboBox->currentIndex();

    niDialog->resize(niDialog->width(), niDialog->maximumHeight()/2);
    ni->nameLineEdit->setFocus();

    dialogType = INSTRUMENT_TYPE;

    QString previousName = ni->nameLineEdit->text();
    bool invalid = true;
    int ret = QDialog::Rejected;
    while(invalid)
    {
        ret = niDialog->exec();

        if(ret == QDialog::Rejected) invalid = false;
        else if(!ni->nameLineEdit->text().isEmpty())
        {
            //Check whether valid instrument name;
            FilterProxy *proxy = ((FilterProxy *)atm->instrumentProxy.data());
            int i = 0;
            for(i = 0; i < proxy->rowCount(); ++i)
            {
                if(proxy->data(proxy->index(i, NAME)).toString() == ni->nameLineEdit->text()
                        && previousName != ni->nameLineEdit->text())
                    break;
            }

            if(i == proxy->rowCount())
            {
                invalid = false;
            }
        }

        if(invalid)
        {
            QMessageBox::critical(this, "Invalid Name", "Please Enter a Unique Name for this instrument");
        }
    }

    return ret;
}

void AssetManager::newInstrument()
{
    niDialog->setWindowTitle("New Instrument");
    ni->nameLineEdit->setText("");
    ni->descriptionPlainTextEdit->setPlainText("");


    if(showInstrumentDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

        //update the db

        FilterProxy *proxy = ((FilterProxy *)atm->instrumentProxy.data());
        proxy->insertRow(proxy->rowCount());
        proxy->setData(proxy->index(proxy->rowCount() - 1, NAME), ni->nameLineEdit->text());
        proxy->setData(proxy->index(proxy->rowCount() - 1, DESCRIPTION), ni->descriptionPlainTextEdit->toPlainText());

        //risk roi cppu
        FilterProxy *rproxy = ((FilterProxy *)atm->riskProxy.data());
        proxy->setData(proxy->index(proxy->rowCount() - 1,INSTRUMENT_RISK), rproxy->data(rproxy->index(ni->riskComboBox->currentIndex(), ID)));
        proxy->setData(proxy->index(proxy->rowCount() - 1, INSTRUMENT_ROI), ni->rateOfInterestDoubleSpinBox->text().toDouble());
        proxy->setData(proxy->index(proxy->rowCount() - 1, INSTRUMENT_CURRENT_PPU), ni->currentPriceDoubleSpinBox->text().toDouble());

        atm->resetDirtyState(true);
        atm->instrumentModel->submitAll();

        atm->sortAll();

        expandAll();
        //updateTreeView();
    }
}

int AssetManager::showTransactionDialog()
{
    ni->transactionGroupBox_1->show();
    ni->transactionGroupBox_2->show();
    ni->currentPriceGroupBox->hide();

    if(state == MINIMAL)
    {
        ni->category->hide();
        ni->categoryComboBox->hide();
    }
    else
    {
        ni->category->show();
        ni->categoryComboBox->show();
    }

    ni->riskGroupBox->show();
    ni->rateGroupBox->show();

    ni->basicInformation->show();

    ni->riskComboBox->setDisabled(true);

    niDialog->resize(niDialog->width(), niDialog->maximumHeight());
    ni->nameLineEdit->setFocus();
    instrumentSelected(ni->instrumentComboBox->currentIndex());

    dialogType = TRANSACTIONS_TYPE;

    bool invalid = true;
    int ret = QDialog::Rejected;
    while(invalid)
    {
        ret = niDialog->exec();

        if(ret == QDialog::Rejected
                || !ni->nameLineEdit->text().trimmed().isEmpty()) invalid = false;
        else
        {
            QMessageBox::critical(this, "Empty Name", "Please Enter a Name for this transaction");
        }
    }

    return ret;
}

void AssetManager::newTransaction()
{
    niDialog->setWindowTitle("New Transaction");
    ni->nameLineEdit->setText("");
    ni->descriptionPlainTextEdit->setPlainText("");
    ni->unitsDoubleSpinBox->setValue(1);

    if(showTransactionDialog() == QDialog::Accepted)
    {
        //ui->treeView->setModel(0);

        if(state == MINIMAL)
        {
            QModelIndex initialIndex = atm->getIndex(0,0, QModelIndex(), true);
            QModelIndex rootIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);

            atm->insertRow(atm->rowCount(rootIndex), rootIndex);

            initialIndex = atm->getIndex(0,0, QModelIndex(), true);
            rootIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);

            QModelIndex userAccountIDIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), ID, initialIndex, true);
            FilterProxy *proxy = (FilterProxy *)atm->instrumentProxy.data();

            int row = atm->rowCount(rootIndex) - 1;

            atm->setData(atm->getIndex(row, NAME, rootIndex, true), ni->nameLineEdit->text());
            atm->setData(atm->getIndex(row, DESCRIPTION, rootIndex, true), ni->descriptionPlainTextEdit->toPlainText());

            atm->setData(atm->getIndex(row, USER_ACCOUNT, rootIndex, true), atm->data(userAccountIDIndex));
            atm->setData(atm->getIndex(row, INSTRUMENT, rootIndex, true), proxy->data(proxy->index(ni->instrumentComboBox->currentIndex(), ID)));

            atm->setData(atm->getIndex(row, RISK, rootIndex, true), ni->riskComboBox->currentIndex());

            atm->setData(atm->getIndex(row, BOUGHT_DATE, rootIndex, true), ni->boughtDateEdit->date());
            atm->setData(atm->getIndex(row, END_DATE, rootIndex, true), ni->endDateEdit->date());

            atm->setData(atm->getIndex(row, RATE_OF_INTEREST, rootIndex, true), ni->rateOfInterestDoubleSpinBox->value());
            atm->setData(atm->getIndex(row, PRICE_PER_UNIT, rootIndex, true), ni->pricePerUnitDoubleSpinBox->value());
            atm->setData(atm->getIndex(row, UNITS, rootIndex, true), ni->unitsDoubleSpinBox->value());
        }
        else
        {
            QModelIndex initialIndex = atm->getIndex(0,0, QModelIndex(), true);
            QModelIndex userAccountIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);
            QModelIndex categoryIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), 0, userAccountIndex, true);

            QModelIndex rootIndex;

            if(state == NORMAL && hideRisk == true)
                rootIndex = categoryIndex;
            else
                rootIndex = atm->getIndex(ni->riskComboBox->currentIndex(),0, categoryIndex, true);

            atm->insertRow(atm->rowCount(rootIndex), rootIndex);

            initialIndex = atm->getIndex(0,0, QModelIndex(), true);
            userAccountIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), 0, initialIndex, true);
            categoryIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), 0, userAccountIndex, true);

            if(state == NORMAL && hideRisk == true)
                rootIndex = categoryIndex;
            else
            rootIndex = atm->getIndex(ni->riskComboBox->currentIndex(),0, categoryIndex, true);

            QModelIndex userAccountIDIndex = atm->getIndex(ni->userAccountComboBox->currentIndex(), ID, initialIndex, true);
            QModelIndex categoryIDIndex = atm->getIndex(ni->categoryComboBox->currentIndex(), ID, userAccountIndex, true);

            FilterProxy *proxy = (FilterProxy *)atm->instrumentProxy.data();

            int row = atm->rowCount(rootIndex) - 1;

            //qDebug()<<"Row Count after insert"<<row;
            atm->setData(atm->getIndex(row, NAME, rootIndex, true), ni->nameLineEdit->text());
            atm->setData(atm->getIndex(row, DESCRIPTION, rootIndex, true), ni->descriptionPlainTextEdit->toPlainText());

            atm->setData(atm->getIndex(row, USER_ACCOUNT, rootIndex, true), atm->data(userAccountIDIndex));
            atm->setData(atm->getIndex(row, CATEGORY, rootIndex, true), atm->data(categoryIDIndex));
            atm->setData(atm->getIndex(row, RISK, rootIndex, true), ni->riskComboBox->currentIndex());
            atm->setData(atm->getIndex(row, INSTRUMENT, rootIndex, true), proxy->data(proxy->index(ni->instrumentComboBox->currentIndex(), ID)));

            atm->setData(atm->getIndex(row, BOUGHT_DATE, rootIndex, true), ni->boughtDateEdit->date());
            atm->setData(atm->getIndex(row, END_DATE, rootIndex, true), ni->endDateEdit->date());

            atm->setData(atm->getIndex(row, RATE_OF_INTEREST, rootIndex, true), ni->rateOfInterestDoubleSpinBox->value());
            atm->setData(atm->getIndex(row, PRICE_PER_UNIT, rootIndex, true), ni->pricePerUnitDoubleSpinBox->value());
            atm->setData(atm->getIndex(row, UNITS, rootIndex, true), ni->unitsDoubleSpinBox->value());
            //updateTreeView();
        }

        atm->transactionsModel->submitAll();

        atm->sortAll();

        expandAll();
    }
}


/****************************************************************/
QWidget *TextColumnDelegate::createEditor(QWidget *parent,
                      const QStyleOptionViewItem &option,
                      const QModelIndex &index) const
{
    QLineEdit  *editor = new QLineEdit (parent);

    editor->setText(index.data().toString());

    return editor;
}

QString TextColumnDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    return value.toString();
}

void TextColumnDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    QString value = index.data().toString();

    QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    lineEdit->setText(value);
}

void TextColumnDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
 {
     QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
     QString value = lineEdit->text();

     model->setData(index, value, Qt::EditRole);
 }


QWidget *DateColumnDelegate::createEditor(QWidget *parent,
                      const QStyleOptionViewItem &option,
                      const QModelIndex &index) const
{
    QDateEdit  *editor = new QDateEdit (parent);

    QDate date = index.data().toDate();
    date = date.isValid()? date : QDate::currentDate();
    editor->setDate(date);
    editor->setCalendarPopup(true);
    editor->setDisplayFormat("dd-MM-yyyy");

    return editor;
}

QString DateColumnDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    return value.toDateTime().toString("dd MMM yyyy");
}

void DateColumnDelegate::paint(QPainter *painter,
           const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QVariant data = index.data(Qt::BackgroundRole);
    QColor color = QPalette().color(QPalette::Base);

    if(data.isValid())
    {
        color = data.value<QColor>();
    }

    painter->fillRect(option.rect, color);
    QStyledItemDelegate::paint(painter, option, index);
}

QWidget *DoubleValueColumnDelegate::createEditor(QWidget *parent,
                      const QStyleOptionViewItem &option,
                      const QModelIndex &index) const
{
    QDoubleSpinBox  *editor = new QDoubleSpinBox (parent);
    //qDebug()<<"Imhere 222";

    editor->setDecimals(4);
    if(index.column() == RATE_OF_INTEREST)
        editor->setDecimals(2);

    editor->setMaximum(9999999999.9999);
    return editor;
}

QString DoubleValueColumnDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    return value.toString();
}

void DoubleValueColumnDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    double value = index.data().toDouble();

    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    spinBox->setValue(value);
}

void DoubleValueColumnDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
 {
     QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
     spinBox->interpretText();
     double value = spinBox->value();

     model->setData(index, value, Qt::EditRole);
 }
