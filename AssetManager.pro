#-------------------------------------------------
#
# Project created by QtCreator 2012-09-17T13:13:12
#
#-------------------------------------------------

QT       += core gui sql

TARGET = AssetManager
TEMPLATE = app


SOURCES += main.cpp\
        assetmanager.cpp \
    assettreemodel.cpp \
    nightcharts.cpp \
    assetpietab.cpp \
    assetfilter.cpp

HEADERS  += assetmanager.h \
    commoninitializers.h \
    assettreemodel.h \
    nightcharts.h \
    assetpietab.h \
    assetfilter.h

FORMS    += assetmanager.ui \
    newinformation.ui \
    chartoptions.ui

RESOURCES += \
    resource.qrc

RC_FILE = AssetManager.rc
