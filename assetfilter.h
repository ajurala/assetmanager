#ifndef ASSETFILTER_H
#define ASSETFILTER_H

#include "assettreemodel.h"
#include "assetpietab.h"

class AssetFilter : public QObject
{
    Q_OBJECT
public:
    AssetFilter(FilterProxy *dProxy, int dColumn, FilterProxy *gProxy, int gColumn);
    ~AssetFilter();

    QWidget *getTab();
    FilterProxy *getProxy();
    int getGroupingColumn();
    int getDataColumn();

    void update();

    void setChartType(Nightcharts::type chartType);
    void setLegend(Nightcharts::legend_type legend);

private slots:

    void dataChanged ( const QModelIndex & topLeft, const QModelIndex & bottomRight );
    void layoutChanged();
    void modelReset ();
    void rowsInserted ( const QModelIndex & parent, int start, int end );
    void rowsMoved ( const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationRow );
    void rowsRemoved ( const QModelIndex & parent, int start, int end );

    void tabDestroyed(QObject *obj);

private:
    void updateChart();

private:
    AssetPieTab *tab;

    QMap<int, double> assetInfo;

    FilterProxy *dataProxy;
    int dataColumn;

    FilterProxy *groupingProxy;
    int groupingColumn;

    static QStringList colorList;
};

#endif // ASSETFILTER_H
