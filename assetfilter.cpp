#include "assetfilter.h"

QStringList AssetFilter::colorList;

AssetFilter::AssetFilter(FilterProxy *dProxy, int dColumn, FilterProxy *gProxy, int gColumn)
{
    dataProxy = dProxy;
    dataColumn = dColumn;

    groupingProxy = gProxy;
    groupingColumn = gColumn;

    tab = new AssetPieTab();


    if(colorList.isEmpty())
    {
        colorList = QColor::colorNames ();

        colorList.removeAll("white");
        colorList.removeAll("antiquewhite");
        colorList.removeAll("bisque");
        colorList.removeAll("ivory");
        colorList.removeAll("ghostwhite");
        colorList.removeAll("navajowhite");
        colorList.removeAll("whitesmoke");
        colorList.removeAll("black");
        colorList.removeAll("aquamarine");
        colorList.removeAll("aqua");
        colorList.removeAll("blanchedalmond");
        colorList.removeAll("cornsilk");
        colorList.removeAll("moccasin");
        colorList.removeAll("darkturquoise");
        colorList.removeAll("floralwhite");
        colorList.removeAll("aliceblue");
        colorList.removeAll("azure");
        colorList.removeAll("beige");
        colorList.removeAll("linen");
        colorList.removeAll("oldlace");
        colorList.removeAll("papayawhip");
        colorList.removeAll("peachpuff");
        colorList.removeAll("seashell");

        colorList.removeOne("lightslategray");
        colorList.removeOne("darkslategray");
        colorList.removeOne("dimgray");
        colorList.removeOne("gray");

        colorList.move(colorList.indexOf("red"), 0);
        colorList.move(colorList.indexOf("green"), 1);
        colorList.move(colorList.indexOf("orange"), 2);
        colorList.move(colorList.indexOf("fuchsia"), 3);
        colorList.move(colorList.indexOf("gold"), 4);
        colorList.move(colorList.indexOf("tomato"), 5);

        //qDebug()<<colorList;
    }

    //Connections
    connect(dataProxy, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChanged(QModelIndex,QModelIndex)));
    connect(dataProxy, SIGNAL(layoutChanged()), this, SLOT(layoutChanged()));
    connect(dataProxy, SIGNAL(modelReset ()), this, SLOT(modelReset ()));
    connect(dataProxy, SIGNAL(rowsInserted(const QModelIndex, int, int)), this, SLOT(rowsInserted(const QModelIndex, int, int)));
    connect(dataProxy, SIGNAL(rowsRemoved(const QModelIndex, int, int)), this, SLOT(rowsRemoved(const QModelIndex, int, int)));
    connect(dataProxy, SIGNAL(rowsMoved ( const QModelIndex, int, int, const QModelIndex, int )), this, SLOT(rowsMoved ( const QModelIndex, int, int, const QModelIndex, int )));

    connect(tab, SIGNAL(destroyed(QObject *)), this, SLOT(tabDestroyed(QObject*)));

    updateChart();
}

AssetFilter::~AssetFilter()
{
    //if(groupingProxy && groupingProxy != dataProxy)
      //  delete groupingProxy;

    if(dataProxy)
        delete dataProxy;

    //qDebug()<<"AssetFilter is dying";
    //delete tab;
}

void AssetFilter::tabDestroyed(QObject *obj)
{
    if(obj == tab)
        deleteLater();
}

void AssetFilter::update()
{
    tab->update();
}

void AssetFilter::setChartType(Nightcharts::type chartType)
{
    tab->setPieType(chartType);
}

void AssetFilter::setLegend(Nightcharts::legend_type legend)
{
    tab->setLegendType(legend);
}


void AssetFilter::updateChart()
{
    double totalInvestment = 0;


    assetInfo.clear();
    tab->clearItemList();

        for(int i = 0; i < dataProxy->rowCount(); ++i)
        {
            /* Depending on the filter Ids, i have to decide upon the Names to Use */
            double data = dataProxy->data(dataProxy->index(i, dataColumn)).toDouble();
            int groupingKey = dataProxy->data(dataProxy->index(i, groupingColumn)).toInt();
            assetInfo[groupingKey] += data;

            totalInvestment += data;
        }

        QMapIterator<int, double> i(assetInfo);
        int j = 0;
        while (i.hasNext()) {
            i.next();

            double percent = (i.value() / totalInvestment) * 100.0;
            QColor color = j < colorList.size() ? QColor(colorList[j]) : QColor(qrand()%256, qrand()%256, qrand()%256);
            QString name = groupingProxy->getData(i.key(), NAME).toString();

            tab->addItemInfo(i.value(), percent, name, color);

            //qDebug()<<j<<i.value()<<percent<<name<<color;

            j++;
        }

    update();
}

void AssetFilter::dataChanged( const QModelIndex & topLeft, const QModelIndex & bottomRight )
{
    if(topLeft.column() <= dataColumn
            && bottomRight.column() >= dataColumn)
        updateChart();
}

void AssetFilter::layoutChanged()
{
    updateChart();
}

void AssetFilter::modelReset()
{
    updateChart();
}

void AssetFilter::rowsInserted( const QModelIndex & parent, int start, int end )
{
    updateChart();
}

void AssetFilter::rowsMoved( const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationRow )
{
    updateChart();
}

void AssetFilter::rowsRemoved( const QModelIndex & parent, int start, int end )
{
    updateChart();
}

QWidget * AssetFilter::getTab()
{
    return tab;
}

FilterProxy * AssetFilter::getProxy()
{
    return dataProxy;
}

int AssetFilter::getGroupingColumn()
{
    return groupingColumn;
}

int AssetFilter::getDataColumn()
{
    return dataColumn;
}

